#include "algo.h"

void algo_init () {
    algout = stdout;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                2. Funkcje dodatkowe.                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Zamiana dwóch liczb całkowitych.

    void swapInts (int* a, int* b) {
        int temp;
        temp = *a;
        *a = *b;
        *b = temp;
    }

// Zamiana dwóch zmiennych znakowych.

    void swapChars (char* a, char* b) {
        char temp;
        temp = *a;
        *a = *b;
        *b = temp;
    }

// Zamiana dwóch liczb zmiennoprzecinkowych.

    void swapFloats (float* a, float* b) {
        float temp;
        temp = *a;
        *a = *b;
        *b = temp;
    }

// Przeliczenie liczby cyfr w danej liczbie.

    int intLen (int number) {
        int result = 1;
        if (number < 0) {
            number *= -1;
            result++;
        }

        if (number >= 1000000000)
            return 10;
        if (number == 0)
            return 1;

        int i;
        for (i = 10; i <= 1000000000; i *= 10) {
            if (number >= (i / 10) && number < i)
                return result;
            result++;
        }

        return result;
    }

// Funkcja losuje liczbę całkowitą z podanego przedziału.

    int getRandomInt (int from, int to) {
        if (from > to)
            swapInts (&from, &to);

        int result = (rand() % ((to + 1) - from)) + from;

        return result;
    }

// Funkcja losuje liczbę zmiennoprzecinkową z podanego przedziału.

    float getRandomFloat (float from, float to) {
        float temp;
        if (from > to)
            swapFloats(&from, &to);

        temp = from;
        to = to - from;
        from = 0;

        float result = ((float)rand()/(float)(RAND_MAX/to)) + temp;

        return result;
    }

// Funkcja mieszająca tablicę całkowicie losowo.

    void shuffleNaive (int* array, int length, int numberOfShuffles) {
        int i;
        for (i = 0; i < numberOfShuffles; i++)
            swapInts(&array[getRandomInt(0, length - 1)], &array[getRandomInt(0, length - 1)]);
    }

// Funkcja mieszająca tablicę algorytmem Fishera-Yatesa.

    void shuffleFisherYates (int* array, int length) {
        length--;
        while (length) {
            swapInts(&array[getRandomInt(0, length - 1)], &array[length]);
            length--;
        }
    }

// Wypelnienie tabliy losowymi elementami.

    void randFill (int* array, int length, int from, int to) {
        if (from > to)
            swapInts (&from, &to);
        int i;
        for (i = 0; i < length; i++)
            array[i] = (rand() % ((to + 1) - from)) + from;
    }

// Wypelnienie tablicy dwuwymiarowej losowymi elementami.

    void randFillMatrix (int** matrix, int lines, int columns, int from, int to) {
        if (from > to)
            swapInts (&from, &to);
        int l, c;
        for (l = 0; l < lines; l++)
            for (c = 0; c < columns; c++)
                matrix[l][c] = (rand() % ((to + 1) - from)) + from;
    }

// Znalezienie losowych składników sumy danej liczby.

    int* getRandomSumComponents (int componentsAmount, int sum, int minValue, int maxValue) {
        int* result = malloc (sizeof(int) * componentsAmount);
        int i;
        int currentSum = 0;
        int difference, addition, randomIndex;

        if (minValue >= (sum /componentsAmount))
            for (i = 0; i < componentsAmount; i++)
                result[i] = (sum / componentsAmount);

        else {
            for (i = 0; i < componentsAmount; i++)
                result[i] = getRandomInt(minValue, maxValue);

            for (i = 0; i < componentsAmount; i++)
                currentSum += result[i];

            if (currentSum / sum != 0)
                for (i = 0; i < componentsAmount; i++)
                    result[i] /= (currentSum / sum);
        }

        currentSum = 0;
        for (i = 0; i < componentsAmount; i++)
            currentSum += result[i];

        difference = currentSum - sum;

        while (difference != 0) {
            addition = getRandomInt(0, difference);
            randomIndex = getRandomInt(0, (componentsAmount - 1));
            if (result[randomIndex] - addition >= minValue && result[randomIndex] - addition <= maxValue) {
                result[randomIndex] -= addition;
                difference -= addition;
            }
        }

        return result;
    }

// Zadeklarowanie pamięci dla kolumn tablicy dwuwymiarowej przy użyciu funkcji malloc.

    void expandMatrix (int** matrix, int lines, int columnsToCreate) {
        int l;
        for (l = 0; l < lines; l++)
            matrix[l] = (int*) malloc (sizeof(int) * columnsToCreate);
    }

// Przepisanie tablicy dwuwymiarowej do tablicy jednowymiarowej.

    void projectMatrix (int** matrix, int lines, int columns, int* result, char leftToRight, char upToDown) {
        int l, c;
        int newIndexA, newIndexB;

        for (l = 0; l < lines; l++)
            for (c = 0; c < columns; c++) {
                if (upToDown) newIndexA = l;
                else newIndexA = (lines - l - 1);
                if (leftToRight) newIndexB = c;
                else newIndexB = (columns - c - 1);

                result[columns * newIndexA + newIndexB] = matrix[l][c];
            }
    }

// Przepisanie tablicy jednowymiarowej do tablicy dwuwymiarowej.

    void reprojectMatrix (int* array, int lines, int columns, int** result, char leftToRight, char upToDown) {
        int l, c;
        int newIndexA, newIndexB;

        for (l = 0; l < lines; l++)
            for (c = 0; c < columns; c++) {
                if (upToDown) newIndexA = l;
                else newIndexA = (lines - l - 1);
                if (leftToRight) newIndexB = c;
                else newIndexB = (columns - c - 1);

                result[l][c] = array[columns * newIndexA + newIndexB];
            }
    }

// Specyficzne (po skosie) przepisanie tablicy dwuwymiarowej do tablicy jednowymiarowej.

    void projectMatrixFromCorner (int** matrix, int lines, int columns, int* result) {
        int x, y;
        int l, c;
        int index = 0;

        for (y = 0; y < columns; y++) {
            l = 0;
            for (c = y; c >= 0 && l < lines; c--) {
                result[index] = matrix[l][c];
                l++;
                index++;
            }
        }

        for (x = 1; x < lines; x++) {
            c = columns - 1;
            for (l = x; l < lines && c >= 0; l++) {
                result[index] = matrix[l][c];
                c--;
                index++;
            }
        }
    }

// Specyficzne (po skosie) przepisanie tablicy jednowymiarowej do tablicy dwuwymiarowej.

    void reprojectMatrixToCorner (int* array, int lines, int columns, int** result) {
        int x, y;
        int l, c;
        int index = 0;

        for (y = 0; y < columns; y++) {
            l = 0;
            for (c = y; c >= 0 && l < lines; c--) {
                result[l][c] = array[index];
                l++;
                index++;
            }
        }

        for (x = 1; x < lines; x++) {
            c = columns - 1;
            for (l = x; l < lines && c >= 0; l++) {
                result[l][c] = array[index];
                c--;
                index++;
            }
        }
    }

// Wyświetlenie tablicy wg schematu podanego na zajęciach.

    void showArray (int* array, int length) {
        if (length == 0)
            fprintf(algout, "[");

        int i, chars = 1;
            fprintf(algout, "[");
            for (i = 0; i < length; i++) {
                if (chars + intLen(array[i]) + 2 > 80) {
                    fprintf(algout, "\n ");
                    chars = 1;
                }
                fprintf(algout, " %d,", array[i]);
                chars += intLen(array[i]) + 2;
            }
            fprintf(algout, "\b ]");
    }

// Wyświetlenie tablicy dwuwymiarowej wg schematu podanego na zajęciach.

    void showMatrix (int** matrix, int lines, int columns) {
        if (columns == 0 || lines == 0) {
            fprintf(algout, "[ ]");
            return;
        }

        int l, c, chars;
            for (l = 0; l < lines; l++) {
                chars = 1;
                fprintf(algout, "[");
                for (c = 0; c < columns; c++) {
                    if (chars + intLen(matrix[l][c]) + 2 > 78) {
                        fprintf(algout, " -\n ");
                        chars = 1;
                    }
                    fprintf(algout, " %d,", matrix[l][c]);
                    chars += intLen(matrix[l][c]) + 2;
                }
                if (l != lines - 1) fprintf(algout, "\b ]\n");
            }
            fprintf(algout, "\b ]");
    }

// Wyświetlenie wyników wyszukiwania tekstu.

    void showStringSearchResults (char* string, int* result) {
        int i;
        puts(string);
        if (result[0] == 0)
            fprintf(algout, "Brak trafień!");
        for (i = 1; i <= result[0]; i++) {
            int j;
            if (i == 1) for (j = 0; j < result[i]; j++)
                fprintf(algout, " ");
            else for (j = result[i - 1] + 1; j < result[i]; j++) {
                fprintf(algout, " ");
            }
            fprintf(algout, "^");
        }
    }

// Wyświetlenie dokładnych wyników wyszukiwania tekstu.

    void showStringSearchWholeResults (char* string, int* result, int patternLength) {
        int i;
        puts(string);
        if (result[0] == 0)
            fprintf(algout, "Brak trafień!");
        for (i = 1; i <= result[0]; i++) {
            int j;
            if (i == 1) for (j = 0; j < result[i]; j++)
                fprintf(algout, " ");
            else {
                for (j = result[i - 1] + 1; j < result[i]; j++)
                    fprintf(algout, " ");
                for (j = 0; j < patternLength - 1; j++)
                    fprintf(algout, "\b");
            }
            for (j = 0; j < patternLength; j++) fprintf(algout, "^");
        }
    }

// Wyświetlenie znaków zakodowanych w tablicy wg schematu podanego na zajęciach.

    void showArrayAsChars (int* array, int length) {
        if (length == 0) fprintf(algout, "[");
        int i, chars = 1;
            fprintf(algout, "[ ");
            for (i = 0; i < length; i++) {
                if (chars + intLen(array[i]) + 2 > 80) {
                    fprintf(algout, "\n ");
                    chars = 1;
                }
                fprintf(algout, "%c", array[i]);
                chars += intLen(array[i]) + 2;
            }
            fprintf(algout, " ]");
    }

// Wyświetlenie znaków zakodowanych w tablicy dwuwymiarowej wg schematu podanego na zajęciach.

    void showMatrixAsChars (int** matrix, int lines, int columns) {
        if (columns == 0 || lines == 0) {
            fprintf(algout, "[ ]");
            return;
        }

        int l, c, chars;
            for (l = 0; l < lines; l++) {
                chars = 1;
                fprintf(algout, "[");
                for (c = 0; c < columns; c++) {
                    if (chars + intLen(matrix[l][c]) + 2 > 78) {
                        fprintf(algout, " -\n ");
                        chars = 1;
                    }
                    fprintf(algout, " %c,", matrix[l][c]);
                    chars += intLen(matrix[l][c]) + 2;
                }
                if (l != lines - 1) fprintf(algout, "\b ]\n");
            }
            fprintf(algout, "\b ]");
    }

// Odwrócenie tablicy (ostatni element jest pierwszym itd.).

    void reverseArray (int* array, int length) {
        int limA, limB;
        if (length == 1) return;
        if (length % 2 == 0) {
            limA = length / 2 - 1;
            limB = length / 2;
        }
        if (length % 2 != 0) {
            limA = length / 2;
            limB = length / 2 + 2;
        }

        int i = 0, j = length - 1;

        while (i <= limA && j >= limB) {
            swapInts(&array[i], &array[j]);
            i++;
            j--;
        }
    }

// Porównanie dwóch tablic.

    int cmpArrays (int* arrayA, int* arrayB, int length) {
        int i;
        int result = 1;

        for (i = 0; i < length && result != 0; i++)
            if (arrayA[i] != arrayB[i])
                result = 0;

        return result;
    }

// Porównanie dwóch tablic i zapisanie indeksów elementów, które się od siebie różnią.

    int* cmpArraysSaveDif (int* arrayA, int* arrayB, int length) {
        int* result = (int*) malloc (sizeof(int));
        result[0] = 0;
        int i;

        for (i = 0; i < length; i++) {
            if (arrayA[i] != arrayB[i]) {
                result[0]++;
                result = (int*) realloc (result, sizeof(int) * result[0] + sizeof(int));
                result[result[0]] = i;
            }
        }

        return result;
    }

// Przeszukiwanie całej tablicy w celu odnalezienia jej maksymalnego / minimalnego elementu.

    int getExtreme (int* array, int length, char getMax) {
        int i;
        int extremeValue = array[0];

            for (i = 1; i < length; i++)
                if ((array[i] > extremeValue && getMax) || (array[i] < extremeValue && !getMax))
                    extremeValue = array[i];

        return extremeValue;
    }

// Przesunięcie elementów tablicy.

    void shiftArray (int* array, int length, int value) {
        char direction = 'r';
        if (value < 0) {
            value *= -1;
            direction = 'l';
        }

        if (value >= length)
            value = value % length;

        if (direction == 'l')
            value = length - value;

        if (value == 0)
            return;

        int* stack = malloc(sizeof(int) * value);
        int i;
        int j = value - 1;

        for (i = length - 1; i >= 0; i--) {
            if (j >= 0) {
                stack[j] = array[i];
                j--;
            }
            array[i] = array[i - value];
        }

        for (i = 0; i < value; i++)
            array[i] = stack[i];

        free(stack);
    }

// Funkcja liczy i zwraca czas sortowania danym algorytmem.

    int getSortTime (void(*sortingMethod)(int*, int, char), int* array, int length, char isAscending) {
        int executionTime = time(NULL);
        sortingMethod(array, length, isAscending);
        return (time(NULL) - executionTime);
    }

// Dobranie końcówek do słów "godzin", "minut" i "sekund" w zależności od ich liczby.

    char* getInfixes (int hours, int minutes, int seconds) {
        char *result = (char*) malloc (sizeof(char) * 3);

        // Odmiana godzin.
        if (hours == 0) result[0] = '\0';
        if (hours == 1) result[0] = 'a';
        if (hours > 1 && hours < 5) result[0] = 'y';
        if (hours > 4 && hours < 22) result[0] = '\0';
        if (hours > 21 && hours < 25) result[0] = 'y';

        // Odmiana minut.
        if (minutes == 0) result[1] = '\0';
        if (minutes == 1) result[1] = 'a';
        if (minutes > 1 && minutes < 5) result[1] = 'y';
        if (minutes > 4 && minutes < 20) result[1] = '\0';
        if (minutes > 19 && ((minutes % 10 == 2) || (minutes % 10 == 3) || (minutes % 10 == 4))) result[1] = 'y';
        else if ((minutes > 19) && !((minutes % 10 == 2) || (minutes % 10 == 3) || (minutes % 10 == 4))) result[1] = '\0';

        // Odmiana sekund (taka sama jak odmiana minut).
        if (seconds == 0) result[2] = '\0';
        if (seconds == 1) result[2] = 'a';
        if (seconds > 1 && seconds < 5) result[2] = 'y';
        if (seconds > 4 && seconds < 20) result[2] = '\0';
        if (seconds > 19 && ((seconds % 10 == 2) || (seconds % 10 == 3) || (seconds % 10 == 4))) result[2] = 'y';
        else if ((seconds > 19) && !((seconds % 10 == 2) || (seconds % 10 == 3) || (seconds % 10 == 4))) result[2] = '\0';

        return result;
    }

// Wyświetlenie czasu w formacie "X godzin, Y minut i Z sekund" na podstawie danej liczby
// sekund, dodając do słów "godzin", "minut" oraz "sekund" odpowiednie końcówki.

    void printTime (int seconds) {
        int hours = seconds / 3600;
        int minutes = (seconds - (hours * 3600)) / 60;
        seconds = seconds - (hours * 3600) - (minutes * 60);

        char* infix = (char*) malloc (sizeof(char) * 3);

        infix = getInfixes (hours, minutes, seconds);

        if (hours + minutes + seconds == 0) fprintf(algout, "mniej niż sekunda");
        if (hours > 0) fprintf(algout, "%d godzin%c", hours, infix[0]);
        if (hours > 0 && minutes > 0) fprintf(algout, ", ");
        if (minutes > 0) fprintf(algout, "%d minut%c", minutes, infix[1]);
        if ((hours > 0 || minutes > 0) && seconds > 0) fprintf(algout, " i ");
        if (seconds > 0) fprintf(algout, "%d sekund%c", seconds, infix[2]);
    }

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                           3. Algorytmy sortujące tablicę.                                           *
 *                                                                                                                     *
 *                              Zwracana wartość to czas wykonywania algorytmu w sekundach.                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Sortowanie metodą flagi polskiej.

    void sort01 (char* array, int length, char isAscending) {
        int left = 0;
        int right = length - 1;

        do {
            while ((array[left] == 0 && left < right && isAscending) || (array[left] != 0 && left < right && !isAscending))
                left++;
            while ((array[right] == 1 && left < right && isAscending) || (array[right] != 1 && left < right && !isAscending))
                right--;

            if (left < right)
                swapChars(&(array[left]), &(array[right]));
        } while (left < right);
    }

// Sortowanie metodą flagi francuskiej.

    void sort012 (char* array, int length, char isAscending) {
        int left = -1;
        int middle = 0;
        int right = length;

        while (middle < right) {
            if ((array[middle] == 0 && isAscending) || (array[middle] == 2 && !isAscending)) {
                left++;
                swapChars(&(array[left]), &(array[middle]));
                middle++;
            }

            else if (array[middle] == 1) middle++;

            else {
                right--;
                swapChars(&(array[middle]), &(array[right]));
            }
        }
    }

// Sortowanie bąbelkowe.

    void sortBubble (int* array, int length, char isAscending) {
        int N = length - 1;
        int i;

        for (; N > 0; N--) {
            for (i = 0; i < N; i++) {
                if (((isAscending) && array[i] > array[i + 1]) || ((!isAscending) && array[i] < array[i + 1]))
                    swapInts (&(array[i]), &(array[i + 1]));
            }
        }
    }

// Sortowanie gnoma.

    void sortGnome (int* array, int length, char isAscending) {
        int i = 1;

        while (i < length) {
            if ((array[i] >= array[i - 1] && isAscending) || (array[i] <= array[i - 1] && !isAscending)) ++i;

            else {
                swapInts(&array[i], &array[i - 1]);
                --i;
                if (i == 0) i++;
            }
        }
    }

// Sortowanie przez wstawianie.

    void sortInsertion (int* array, int length, char isAscending) {
        int key;
        int i, j;

        for (i = 1; i < length; ++i) {
            key = array[i];

            for (j = i - 1; j >= 0 && (((isAscending) && array[j] > key) || (!(isAscending) && array[j] < key)); --j) {
                array[j + 1] = array[j];
            }

            array[j + 1] = key;
        }
    }

// Sortowanie przez wybranie minimalnej wartości.

    void sortSelection (int* array, int length, char isAscending) {
        int i, j;
        int iMin;

        for (j = 0; j < length - 1; j++) {
            iMin = j;
            for (i = j + 1; i < length; i++) {
                if (((isAscending) &&array[i] < array[iMin]) || (!(isAscending) &&array[i] > array[iMin])) {
                    iMin = i;
                }
            }
            if (iMin != j) {
                swapInts (&array[j], &array[iMin]);
            }
        }
    }

// Sortowanie grzebieniowe.

    void sortComb (int* array, int length, char isAscending) {
        int gap = length;
        char swapped;
        int i;

        do {
            swapped = 0;

            gap /= 1.3;
            if (gap == 9 || gap == 10) gap = 11;
            if (gap < 1) gap = 1;

            for (i = 0; i < length - gap; ++i) {
                if ((array[i] > array[i + gap] && isAscending) || (array[i] < array[i + gap] && !isAscending)) {
                    swapped = 1;
                    swapInts(&array[i], &array[i + gap]);
                }
            }
        } while (gap > 1 || swapped);
    }

// Sortowanie szybkie.

    void sortQuick (int* array, int length, char isAscending) {
        sortQuickPartially(array, 0, length, isAscending);
    }

// Właściwe sortowanie szybkie. Wygodniejsza w użyciu jest funkcja powyżej.

    void sortQuickPartially (int* array, int begin, int end, char isAscending) {
        int pivotIndex = begin + (end - begin) / 2;
        int pivot;

        if (begin < end) {
            int left = begin + 1;
            int right = end;

            swapInts(&array[begin], &array[pivotIndex]);
            pivot = array[begin];

            while (left < right) {
                if ((array[left] <= pivot && isAscending) || (array[left] >= pivot && !isAscending)) left++;
                else {
                    if (isAscending) while (left < --right && array[right] >= pivot);
                    else while (left < --right && array[right] <= pivot);

                    swapInts(&array[left], &array[right]);
                }
            }

            left--;
            swapInts(&array[begin], &array[left]);

            sortQuickPartially(array, begin, left, isAscending);
            sortQuickPartially(array, right, end, isAscending);
        }
    }

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                        4. Algorytmy przeszukujące tablicę.                                          *
 *                                                                                                                     *
 *                                   Zwracana wartość to indeks szukanego elementu                                     *
 *                                   lub (-1), jeżeli element nie został znaleziony.                                   *
 *                                      Indeks pierwszego elementu tablicy to 0.                                       *
 *                                 Wartością zwracaną przez funkcje przeszukujące ciąg                                 *
 *                                 znaków jest jednowymiarowa tablica indeksów trafień,                                *
 *                                   której pierwszym elementem jest liczba trafień.                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Wyszukiwanie liniowe.

    int searchLinear (int* array, int length, int searchedValue) {
        int i;
            for (i = 0; i < length; i++)
                if (array[i] == searchedValue) return i;

        return (-1);
    }

// Wyszukiwanie liniowe i zapisanie indeksów znalezionych elementów.

    int* searchLinearSaveRes (int* array, int length, int searchedValue) {
        int* result = (int*) malloc (sizeof(int));
        result[0] = 0;

        int i;
            for (i = 0; i < length; i++)
                if (array[i] == searchedValue) {
                    result[0]++;
                    result = (int*) realloc(result, sizeof(int) * result[0] + sizeof(int));
                    result[result[0]] = i;
                }

        return result;
    }

// Wyszukiwanie binarne (tylko na posortowanej tablicy).

    int searchBinary (int* sortedArray, int length, int searchedValue) {
        char isAscending = 1;
            if (sortedArray[0] > sortedArray[length - 1]) isAscending = 0;

        int left = 0;
        int right = length - 1;
        int middle;
        int middleElement;

            while (left <= right) {
                middle = (left + right) / 2;
                middleElement = sortedArray[middle];

                if (middleElement == searchedValue)
                    return middle;

                else if ((middleElement < searchedValue && isAscending) || (middleElement > searchedValue && !isAscending))
                    left = middle + 1;

                else
                    right = middle - 1;
            }
        return (-1);
    }

// Wyszukiwanie interpolacyjne (tylko na posortowanej tablicy).

    int searchInterpolating (int* sortedArray, int length, int searchedValue) {
        char isAscending = 1;
            if (sortedArray[0] > sortedArray[length - 1]) isAscending = 0;

        int left = 0;
        int right = length - 1;
        int middle;
        int middleElement;

            while (left <= right) {
                middle = left + (searchedValue - sortedArray[left]) * (right - left) / (sortedArray[right] - sortedArray[left]);
                middleElement = sortedArray[middle];

                if (middleElement == searchedValue)
                    return middle;

                else if ((middleElement < searchedValue && isAscending) || (middleElement > searchedValue && !isAscending))
                    left = middle + 1;

                else
                    right = middle - 1;
            }
        return (-1);
    }

// Naiwne wyszukiwanie wzorca w danym tekście.

    int* searchStringNaive (char* string, int stringLength, char* pattern, int patternLength) {
        int* result = (int*) malloc (sizeof(int));
        result[0] = 0;
        char isMatching;

        int i;
        for (i = 0; i < stringLength - patternLength + 2; i++) {
            isMatching = 0;
            if (string[i] == pattern[0]) {
                isMatching = 1;
                int j;
                for (j = i + 1; j < i + patternLength && isMatching; j++) {
                    if (string[j] != pattern[j - i])
                        isMatching = 0;
                }
            }
            if (isMatching) {
                result[0]++;
                result = (int*) realloc (result, sizeof(int) * result[0] + sizeof(int));
                result[result[0]] = i;
            }
        }

        return result;
    }

// Wyszukiwanie wzorca w danym tekście algorytmem Karpa-Rabina.

    int* searchStringKR (char* string, int stringLength, char* pattern, int patternLength, int hashingPrime) {
        int* result = (int*) malloc (sizeof(int));
        result[0] = 0;
        int i, j;
        int hashP = 0;
        int hashS = 0;
        int h = 1;

        for (i = 0; i < patternLength-1; i++)
            h = (h * alphabetLength) % hashingPrime;

        for (i = 0; i < patternLength; i++) {
            hashP = (alphabetLength * hashP + pattern[i]) % hashingPrime;
            hashS = (alphabetLength * hashS + string[i]) % hashingPrime;
        }

        for (i = 0; i <= stringLength - patternLength; i++) {
            if (hashP == hashS) {
                for (j = 0; j < patternLength; j++) {
                    if (string[i + j] != pattern[j])
                        break;
                }
                if (j == patternLength) {
                    result[0]++;
                    result = (int*) realloc (result, sizeof(int) * result[0] + sizeof(int));
                    result[result[0]] = i;
                }
            }

            if (i < stringLength - patternLength) {
                hashS = (alphabetLength * (hashS - string[i] * h) + string[i + patternLength]) % hashingPrime;

                if (hashS < 0)
                  hashS = (hashS + hashingPrime);
            }
        }

        return result;
    }

// Wyszukiwanie wzorca w danym tekście algorytmem Knutha-Morrisa-Pratta.

    int* searchStringKMP (char* string, int stringLength, char* pattern, int patternLength) {
        int i;
        int* result = (int*) malloc (sizeof(int));
        result[0] = 0;

        int k = -1;
        i = 1;
        int *prefix = malloc(sizeof(int)*patternLength);

        prefix[0] = k;
        for (i = 1; i < patternLength; i++) {
            while (k > -1 && pattern[k + 1] != pattern[i])
                k = prefix[k];
            if (pattern[i] == pattern[k + 1])
                k++;
            prefix[i] = k;
        }

        k = -1;
        if (!prefix) {
            result[0] = -1;
            return result;
        }
        for (i = 0; i < stringLength; i++) {
            while (k > -1 && pattern[k + 1] != string[i])
                k = prefix[k];
            if (string[i] == pattern[k+1])
                k++;
            if (k == patternLength - 1) {
                result[0]++;
                result = (int*) realloc (result, sizeof(int) * result[0] + sizeof(int));
                result[result[0]] = i - k;
            }
        }

        return result;
    }

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                            5. Funkcje do struktur danych.                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Położenie elementu na stosie.

    void push (int value, Stack** givenStack) {
        Stack* newStack = malloc (sizeof(Stack));

        newStack->value = value;
        newStack->node = *givenStack;

        *givenStack = newStack;
    }

// Pobranie elementu ze stosu.

    char pop (Stack** givenStack, int* value) {
        if (*givenStack == NULL)
            return 0;

        *value = (*givenStack)->value;
        Stack* oldStack = *givenStack;

        *givenStack = oldStack->node;
        free(oldStack);

        return 1;
    }

// Pobranie wartości danego elementu stosu bez niszczenia go.

    char safeGetNodeValue (Stack* givenStack, int depth, int* result) {
        if (depth < 0) depth = 0;
        Stack* current = givenStack;

        int i;

        for (i = 0; current != NULL && i < depth; i++)
            current = current->node;

        if (current == NULL) {
            *result = 0;
            return 0;
        }

        else {
            *result = current->value;
            return 1;
        }
    }

// Przepisanie stosu do tablicy jednowymiarowej.

    void projectStack (Stack** givenStack, int* result) {
        char isFilled = 1;
        int value;

        int i;

        for (i = 0; isFilled; i++) {
            isFilled = pop(givenStack, &value);
            if (isFilled)
                result[i] = value;
        }
    }

// Przepisanie stosu do tablicy jednowymiarowej bez niszczenia stosu.

    void safeProjectStack (Stack* givenStack, int* result) {
        Stack* current = givenStack;

        int i;

        for (i = 0; current != NULL; i++) {
            result[i] = current->value;
            current = current->node;
        }
    }

// Przepisanie tablicy jednowymiarowej do stosu.

    void reprojectStack (int* givenArray, int length, Stack** result) {
        int i;

        for (i = length - 1; i >= 0; i--)
            push(givenArray[i], result);
    }

// Znalezienie wysokości stosu.

    int getStackHeight (Stack* givenStack) {
        int stackHeight;

        Stack* current = givenStack;

        for (stackHeight = 0; current != NULL; stackHeight++) {
            current = current->node;
        }

        return stackHeight;
    }

// Znalezienie wysokości stosu.

    int getStackHeightWorse (Stack** givenStack) {
        Stack* backup = NULL;
        int stackHeight = 0;
        int value = 0;
        char isFilled = 1;

        while (isFilled) {
            isFilled = pop(givenStack, &value);
            if (isFilled) {
                push(value, &backup);
                stackHeight++;
            }
        }

        isFilled = 1;

        while (isFilled) {
            isFilled = pop(&backup, &value);
            if (isFilled)
                push(value, givenStack);
        }

        return stackHeight;
    }

// Odwrócenie stosu.

    void reverseStack (Stack** givenStack) {
        Stack* backup = *givenStack;
        int value = 0;
        char isFilled = 1;

        while (isFilled) {
            isFilled = pop(givenStack, &value);
            if (isFilled)
                push(value, &backup);
        }

        isFilled = 1;

        while (isFilled) {
            isFilled = pop(&backup, &value);
            if (isFilled)
                push(value, givenStack);
        }
    }

// Usunięcie stosu (zwolnienie pamięci przez niego zajmowanej).

    void deleteStack (Stack** givenStack) {
        int temp;
        while (pop(givenStack, &temp));
    }

// Dodanie komórki na tył kolejki.

    Cell* enQueue (int value, Queue** givenQueue) {
        Cell* newCell = calloc (1, sizeof(Cell));
        newCell->value = value;

        newCell->predecessor = NULL;

        if ((*givenQueue)->back != NULL) {
            (*givenQueue)->back->predecessor = newCell;
            newCell->successor = (*givenQueue)->back;
            (*givenQueue)->back = newCell;
            (*givenQueue)->length++;
        }

        else {
            (*givenQueue)->back = newCell;
            (*givenQueue)->front = newCell;
            (*givenQueue)->length = 1;
        }

        return newCell;
    }

// Pobranie komórki z przodu kolejki.

    char deQueue (Queue** givenQueue, int* value) {
        if ((*givenQueue)->front == NULL) {
            *value = 0;
            return 0;
        }

        Cell* newFront = (*givenQueue)->front->predecessor;

        if ((*givenQueue)->front->predecessor != NULL)
            (*givenQueue)->front->predecessor->successor = NULL;

        *value = (*givenQueue)->front->value;

        free((*givenQueue)->front);

        (*givenQueue)->front = newFront;

        (*givenQueue)->length--;

        return 1;
    }

// Znalezienie liczby komórek w kolejce.

    int getQueueLength (Queue* givenQueue) {
        if (givenQueue->back == NULL)
            return 0;
        else
            return givenQueue->length;
    }

// Przepisanie kolejki do tablicy jednowymiarowej bez niszczenia kolejki.

    void projectQueue (Queue* givenQueue, int* result) {
        if (givenQueue->front == NULL)
            return;

        Cell* current = givenQueue->back;
        result[getQueueLength(givenQueue) - 1] = current->value;

        int i;
        for (i = getQueueLength(givenQueue) - 2; i >= 0; i--) {
            current = current->successor;
            result[i] = current->value;
        }
    }

// Przepisanie tablicy jednowymiarowej do kolejki.

    void reprojectQueue (int* givenArray, int length, Queue** result) {
        int i;
        for (i = 0; i < length; i++)
            enQueue(givenArray[i], result);
    }

// Dodanie nowej komórki do kolejki w odniesieniu do innej komórki.

    char addCell (int value, Queue* givenQueue, Cell* referenceCell, char direction) {
        if (direction == 'r' && referenceCell->successor == NULL)
            return 0;
        if (direction == 'l' && referenceCell->predecessor == NULL)
            return 0;

        Cell* newCell = malloc (sizeof(Cell));

        newCell->value = value;

        if (direction == 'r') {
            newCell->predecessor = referenceCell;
            newCell->successor = referenceCell->successor;
            referenceCell->successor->predecessor = newCell;
            referenceCell->successor = newCell;
            givenQueue->length++;
            return 1;
        }

        else if (direction == 'l') {
            newCell->predecessor = referenceCell->predecessor;
            newCell->successor = referenceCell;
            referenceCell->predecessor->successor= newCell;
            referenceCell->predecessor = newCell;
            givenQueue->length++;
            return 1;
        }

        return 0;
    }

// Dodanie komórki z przodu kolejki.

    Cell* addCellToFront (int value, Queue** givenQueue) {
        Cell* newCell = calloc (1, sizeof(Cell));
        newCell->value = value;

        newCell->successor = NULL;

        if ((*givenQueue)->front != NULL) {
            (*givenQueue)->front->successor = newCell;
            newCell->predecessor = (*givenQueue)->front;
            (*givenQueue)->front = newCell;
            (*givenQueue)->length++;
        }

        else {
            (*givenQueue)->back = newCell;
            (*givenQueue)->front = newCell;
            (*givenQueue)->length = 1;
        }

        return newCell;
    }

// Usunięcie kolejki (zwolnienie pamięci przez nią zajmowanej).

    void deleteQueue (Queue** givenQueue) {
        int temp;
        while (deQueue(givenQueue, &temp));
    }

// Dodanie elementu do listy.

    List* insertToList (int value, List* givenList, int predecessorIndex) {
        int i;
        for (i = 0; i != predecessorIndex && givenList->successor != NULL; i++)
            givenList = givenList->successor;

        List* newIndex = malloc (sizeof(List));
        newIndex->value = value;
        newIndex->successor = givenList->successor;
        givenList->successor = newIndex;
        return newIndex;
    }

// Usunicie elementu z listy.

    char deleteFromList (List* givenList, int index) {
        int i;
        for (i = 0; i != index - 1; i++) {
            givenList = givenList->successor;
            if (givenList->successor == NULL && i < index - 1)
                return 0;
        }

        List* toDelete = givenList->successor;
        givenList->successor = givenList->successor->successor;
        free(toDelete);
        return 1;
    }

// Zmiana wartości danego elementu listy.

    int setListValue (int value, List* givenList, int index) {
        int i;
        for (i = 0; i != index && givenList->successor != NULL; i++)
            givenList = givenList->successor;

        int oldValue = givenList->value;
        givenList->value = value;
        return oldValue;
    }

// Pobranie wartości danego elementu listy.

    int getListValue (List* givenList, int index) {
        int i;
        for (i = 0; i != index && givenList->successor != NULL; i++)
            givenList = givenList->successor;

        return givenList->value;
    }

// Zmiana liczby elementów listy.

    char setListLength (List* givenList, int newLength) {
        if (newLength <= 0)
            return 0;

        int i = 0;
        if (givenList != NULL)
            while (givenList->successor != NULL) {
                givenList = givenList->successor;
                i++;
                if (i == newLength - 1) {
                    if (givenList->successor == NULL) {
                        return 0;
                    }
                    else {
                        List* current = givenList->successor;
                        givenList->successor = NULL;
                        while (current != NULL) {
                            List* toDelete = current;
                            current = current->successor;
                            free(toDelete);
                        }
                        return 1;
                    }
                }
            }

        else
            return 0;

        if (i >= newLength - 1)
            return 0;

        for (; i < newLength - 1; i++) {
            List* newIndex = calloc(1, sizeof(List));
            givenList->successor = newIndex;
            givenList = newIndex;
        }

        return 1;
    }

// Znalezienie liczby elementów listy.

    int getListLength (List* givenList) {
        int result = 0;
        while (givenList != NULL)
            result++;

        return result;
    }

// Przepisanie listy do tablicy jednowymiarowej bez niszczenia listy.

    char projectList (List* givenList, int* result) {
        if (givenList == NULL)
            return 0;

        int i;
        for (i = 0; givenList; i++) {
            result[i] = givenList->value;
            givenList = givenList->successor;
        }
        return 1;
    }

// Przepisanie tablicy jednowymiarowej do listy.

    char reprojectList (int* givenArray, int length, List* result) {
        int lastIndex = getListLength(result) - 1;

        int i;
        for (i = 0; i < length; i++) {
            insertToList(givenArray[i], result, lastIndex);
            lastIndex++;
        }
        return 1;
    }

// Dodanie węzła do danego drzewa binarnego.

    BinaryTree* insert (int value, BinaryTree** givenTree) {
        BinaryTree* temp = NULL;

        if(*givenTree == NULL) {
            temp = malloc(sizeof(BinaryTree));
            temp->left = temp->right = NULL;
            temp->value = value;
            *givenTree = temp;
            return temp;
        }

        if(value < (*givenTree)->value)
            return insert(value, &(*givenTree)->left);

        else if(value > (*givenTree)->value)
            return insert(value, &(*givenTree)->right);

        return NULL;
    }

// Znalezienie rozmiaru drzewa (liczby jego elementów).

    int getTreeSize (BinaryTree* givenTree) {
        if (givenTree == NULL)
            return 0;

        int result = 1;

        result += getTreeSize(givenTree->left);
        result += getTreeSize(givenTree->right);

        return result;
    }

// Wyszukiwanie węzła o danej wartości w drzewie binarnym.

    BinaryTree* searchBinaryTree (BinaryTree** givenTree, int searchedValue) {
        if(*givenTree == NULL)
            return NULL;

        if(searchedValue < (*givenTree)->value)
            searchBinaryTree(&((*givenTree)->left), searchedValue);

        else if(searchedValue > (*givenTree)->value)
            searchBinaryTree(&((*givenTree)->right), searchedValue);

        else if(searchedValue == (*givenTree)->value)
            return *givenTree;

        return NULL;
    }

// Przepisanie drzewa binarnego do tablicy jednowymiarowej.

    char projectTree (BinaryTree* givenTree, int* result, int* counter, char order) {
        if (!givenTree)
            return 0;

        if (order == (-1)) {
                ++*counter;
                result[*counter - 1] = givenTree->value;
            projectTree(givenTree->left, result, counter, order);
            projectTree(givenTree->right, result, counter, order);
        }

        if (order == 0) {
            projectTree(givenTree->left, result, counter, order);
                ++*counter;
                result[*counter - 1] = givenTree->value;
            projectTree(givenTree->right, result, counter, order);
        }

        if (order == 1) {
            projectTree(givenTree->left, result, counter, order);
            projectTree(givenTree->right, result, counter, order);
                ++*counter;
                result[*counter - 1] = givenTree->value;
        }

        return 1;
    }

// Dopisanie tablicy jednowymiarowej do drzewa.

    void reprojectTree (int* givenArray, int length, BinaryTree** result) {
        int i;
        for (i = 0; i < length; i++)
            insert(givenArray[i], result);
    }

// Usunięcie drzewa (zwolnienie pamięci przez nie zajmowanej).

    void deleteTree (BinaryTree** givenTree) {
        if ((*givenTree)->left != NULL)
            deleteTree(&(*givenTree)->left);
        if ((*givenTree)->right != NULL)
            deleteTree(&(*givenTree)->right);
        free(*givenTree);
    }

// Źródła:
// http://en.wikibooks.org/wiki/Algorithm_Implementation/Sorting
// http://en.wikibooks.org/wiki/Algorithm_Implementation/Search
// http://algorytmy.blox.pl/2008/03/Wyszukiwanie-interpolacyjne.html
// http://www.geeksforgeeks.org/searching-for-patterns-set-3-rabin-karp-algorithm/
// http://kolos.math.uni.lodz.pl/~archive/Algorytmy%20i%20struktury%20danych%202%20%28Zaawansowane%20algorytmy%29/Projekt%204%20-%20Wzorzec/wyk12.pdf
// http://rosettacode.org/wiki/
// http://stackoverflow.com/questions/6579456/implementation-of-queue-using-pointers-segmentation-error
// http://en.wikipedia.org/wiki/Abstract_data_structure
// https://en.wikipedia.org/wiki/List_%28abstract_data_type%29
// http://en.wikipedia.org/wiki/List_of_data_structures#Binary_trees
// http://www.thegeekstuff.com/2013/02/c-binary-tree/
