/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                            Algorytmy i struktury danych.                                            *
 *                                                     17.02.2015                                                      *
 *                                                                                                                     *
 * 1. Definicje struktur danych:                                                                                       *
 *       - Stack - stos,                                                                                               *
 *       - Cell - komórka kolejki,                                                                                     *
 *       - Queue - kolejka,                                                                                            *
 *       - List - lista jednokierunkowa,                                                                               *
 *       - BinaryTree - drzewo binarne.                                                                                *
 *                                                                                                                     *
 * 2. Funkcje dodatkowe:                                                                                               *
 *       - swapInts - zamiana dwóch liczb całkowitych,                                                                 *
 *       - swapChars - zamiana dwóch zmiennych znakowych,                                                              *
 *       - intLen - przeliczenie liczby znaków w danej liczbie,                                                        *
 *       - getRandomInt - funkcja losuje liczbę całkowitą z podanego przedziału,                                       *
 *       - getRandomFloat - funkcja losuje liczbę zmiennoprzecinkową z podanego przedziału,                            *
 *       - shuffleNaive - funkcja mieszająca tablicę całkowicie losowo,                                                *
 *       - shuffleFisherYates - funkcja mieszająca tablicę algorytmem Fishera-Yatesa,                                  *
 *       - randFill - wypełnienie tablicy losowymi liczbami,                                                           *
 *       - randFillMatrix - wypełnienie tablicy dwuwymiarowej losowymi liczbami,                                       *
 *       - getRandomSumComponents - znalezienie losowych składników sumy danej liczby,                                 *
 *       - expandMatrix - zadeklarowanie pamięci dla kolumn tablicy dwuwymiarowej,                                     *
 *       - projectMatrix - przepisanie tablicy dwuwymiarowej do tablicy jednowymiarowej,                               *
 *       - reprojectMatrix - przepisanie tablicy jednowymiarowej do tablicy dwuwymiarowej,                             *
 *       - projectMatrixFromCorner - specyficzne przepisanie macierzy do tablicy,                                      *
 *       - reprojectMatrixFromCorner - specyficzne przepisanie tablicy do macierzy,                                    *
 *       - showArray - wyświetlenie tablicy,                                                                           *
 *       - showMatrix - wyświetlenie tablicy dwuwymiarowej,                                                            *
 *       - showStringSearchResults - wyświetlenie wyników wyszukiwania tekstu,                                         *
 *       - showStringSearchWholeResults - wyświetlenie dokładnych wyników wyszukiwania tekstu,                         *
 *       - reverseArray - odwrócenie tablicy,                                                                          *
 *       - cmpArrays - porównanie dwóch tablic,                                                                        *
 *       - cmpArraysSaveDif - porównanie dwóch tablic i zapisanie różnic,                                              *
 *       - getExtreme - znalezienie ekstremum tablicy,                                                                 *
 *       - shiftArray - przesunięcie elementów tablicy,                                                                *
 *       - getSortTime - funkcja liczy i zwraca czas sortowania danym algorytmem,                                      *
 *       - getInfixes - dobranie końcówki do słów "godzin", "minut" i "sekund",                                        *
 *       - printTime - wyświetlenie czasu danego w sekundach.                                                          *
 *                                                                                                                     *
 * 3. Algorytmy sortujące:                                                                                             *
 *       - sort01 - flaga polska,                                                                                      *
 *       - sort012 - flaga francuska,                                                                                  *
 *       - sortBubble - bąbelkowe,                                                                                     *
 *       - sortGnome - gnoma,                                                                                          *
 *       - sortInsertion - przez wstawianie,                                                                           *
 *       - sortSelection - przez wybranie minimalnej wartości,                                                         *
 *       - sortComb - sortowanie grzebieniowe,                                                                         *
 *       - sortQuick - sortowanie szybkie.                                                                             *
 *                                                                                                                     *
 * 4. Algorytmy wyszukiwania:                                                                                          *
 *       - searchLinear - liniowe,                                                                                     *
 *       - searchLinearSaveRes - liniowe z zapisaniem trafień,                                                         *
 *       - searchBinary - binarne,                                                                                     *
 *       - searchInterpolating - interpolacyjne,                                                                       *
 *       - searchStringNaive - naiwne wyszukiwanie tekstu,                                                             *
 *       - searchStringKR - wyszukiwanie tekstu algorytmem Karpa-Rabina,                                               *
 *       - searchStringKMP - Knutha-Morrisa-Pratta.                                                                    *
 *                                                                                                                     *
 * 5. Funkcje do struktur danych:                                                                                      *
 *       - push - położenie elementu na stosie,                                                                        *
 *       - pop - pobranie elementu ze stosu,                                                                           *
 *       - safeGetNodeValue - pobranie wartości danego elementu stosu bez niszczenia go,                               *
 *       - projectStack - przepisanie stosu do tablicy jednowymiarowej (stos jest niszczony),                          *
 *       - safeProjectStack - przepisanie stosu do tablicy jednowymiarowej bez niszczenia stosu,                       *
 *       - reprojectStack - przepisanie tablicy jednowymiarowej do stosu,                                              *
 *       - getStackHeight - znalezienie wysokości stosu,                                                               *
 *       - getStackHeightWorse - to co wyżej, tylko sposobem z laborków,                                               *
 *       - reverseStack - odwrócenie stosu,                                                                            *
 *       - deleteStack - usunięcie stosu (zwolnienie pamięci przez niego zajmowanej),                                  *
 *       - enQueue - dodanie komórki na tył kolejki,                                                                   *
 *       - deQueue - pobranie komórki z przodu kolejki,                                                                *
 *       - getQueueLength - znalezienie liczby komórek w kolejce,                                                      *
 *       - projectQueue - przepisanie kolejki do tablicy jednowymiarowej,                                              *
 *       - reprojectQueue - przepisanie tablicy jednowymiarowej do kolejki,                                            *
 *       - addCell - dodanie nowej komórki do kolejki w odniesieniu do innej komórki,                                  *
 *       - addCellToFront - dodanie komórki z przodu kolejki,                                                          *
 *       - deleteQueue - usunięcie kolejki (zwolnienie pamięci przez nią zajmowanej),                                  *
 *       - insertToList - dodanie elementu do listy,                                                                   *
 *       - deleteFromList - usunicie elementu z listy,                                                                 *
 *       - setListValue - zmiana wartości danego elementu listy,                                                       *
 *       - getListValue - pobranie wartości danego elementu listy,                                                     *
 *       - setListLength - zmiana liczby elementów listy,                                                              *
 *       - getListLength - znalezienie liczby elementów listy,                                                         *
 *       - projectList - przepisanie listy do tablicy jednowymiarowej bez niszczenia listy,                            *
 *       - reprojectList - przepisanie tablicy jednowymiarowej do listy,                                               *
 *       - insert - dodanie węzła do danego drzewa binarnego,                                                          *
 *       - getTreeSize - znalezienie rozmiarów drzewa (liczby jego elementów),                                         *
 *       - searchBinaryTree - wyszukiwanie węzła o danej wartości w drzewie binarnym,                                  *
 *       - projectTree - przepisanie drzewa binarnego do tablicy jednowymiarowej,                                      *
 *       - reprojectTree - dopisanie tablicy jednowymiarowej do drzewa,                                                *
 *       - deleteTree - usunięcie drzewa (zwolnienie pamięci przez nie zajmowanej).                                    *
 *                                                                                                                     *
 *    Lista zmian:                                                                                                     *
 *       - 17.02.2015 - podzielenie biblioteki na pliki .c i .h, usunięcie obsługi błędów oraz makr funkcji,           *
 *                      usunięcie struktury Point oraz funkcji na niej działających, zamiana tablic [] na              *
 *                      wskaźniki *, poprawienie wielu funkcji (sort01,sort012, sortBubble, safeGetNodeValue,          *
 *                      safeProjectStack, reprojectStack, deQueue, getTreeSize, intLen, insert),                       *
 *       - 07.07.2014 - dodanie funkcji getRandomSumComponents, poprawienie obsługi błędów (typ ErrorName              *
 *                      zamiast ID błędu),                                                                             *
 *       - 29.06.2014 - dodanie funkcji shuffleFisherYates (makro: xaShuffleArray) oraz shuffleNaive,                  *
 *                      zrefaktoryzowanie nazw struktur danych (od teraz zaczynają się wielką literą), dodanie         *
 *                      w warunkach niektórych funkcji struktur danych zapytanie o NULL (zamiast zwykłego              *
 *                      przyrównania do zera) ze względu na to, że NULL nie zawsze jest równy zeru, poprawienie        *
 *                      funkcji deleteTree, funkcje enQueue, addCellToFront, insertToList oraz insert zwracają         *
 *                      teraz wskaźnik na nowo dodane do struktury elementy,                                           *
 *       - 01.06.2014 - poprawiona została funkcja getRandomInt, dodana struktura Point oraz funkcje                   *
 *                      getDistanceBetweenPoints i getRandomPointInCircle,                                             *
 *       - 30.05.2014 - dodanie obsługi błędów (funkcja errorHandler) oraz funkcji reprojectList, getRandomInt,        *
 *                      getRandomFloat i getSortTime, większość funkcji sortujących nie zwraca już swojego             *
 *                      czasu wykonywania,                                                                             *
 *       - 29.05.2014 - dodanie listy jednokierunkowej oraz funkcji insertToList, deleteFromList, setListValue,        *
 *                      setListValue, setListLength, getListLength, projectList,                                       *
 *       - 13.04.2014 - poprawienie funkcji shiftArray, dodanie pola długości dla kolejki (dzięki temu podczas         *
 *                      mierzenia jej długości nie trzeba zliczać wszystkich elementów - funkcje do kolejek            *
 *                      zostały zmienione), dodanie funkcji projectQueue, reprojectQueue, deleteStack,                 *
 *                      deleteQueue, dodanie makr dla łatwiejszego posługiwania się wybranymi funkcjami (nazwy         *
 *                      makr zaczynają się od "xa"), dodanie definicji drzewa binarnego, dodanie funkcji insert,       *
 *                      getTreeSize, searchBinaryTree, projectTree, reprojectTree oraz deleteTree,                     *
 *       - 11.04.2014 - dodanie definicji stosu i kolejki, dodanie funkcji push, pop, safeGetNodeValue,                *
 *                      projectStack, safeProjectStack, reprojectStack, getStackHeight, reverseStack, enQueue,         *
 *                      deQueue, getQueueLength, addCell oraz addCellToFront, poprawienie komentarzy do                *
 *                      wszystkich funkcji (od teraz przy nazwach zmiennych są też ich typy).                          *
 *       - 03.04.2014 - dodanie stałej alphabetLength, dodanie funkcji searchStringKMP, poprawienie funkcji            *
 *                      cmpArraysSaveDif oraz searchLinearSaveRes tak, aby zapisywały wyniki tylko do jednej           *
 *                      tablicy.                                                                                       *
 *       - 02.04.2014 - dodanie funkcji searchStringNaive, searchStringKR, showStringSearchResults oraz                *
 *                      showStringSearchWholeResults,                                                                  *
 *       - 31.03.2014 - dodanie funkcji reverseArray,                                                                  *
 *       - 30.03.2014 - dodanie funkcji swapChars, intLen, projectMatrixFromCorner i reprojectMatrixFromCorner,        *
 *                      przerobienie funkcji showArray oraz showMatrix tak, aby wyświetlały do 80 znaków na            *
 *                      linię (ze względu na problem z wyświetlaniem dużych liczb),                                    *
 *       - 28.03.2014 - usunięcie funkcji randDoubleFill i randTripleFill, dodanie funkcji randFillMatrix,             *
 *                      createMatrix, projectMatrix, reprojectMatrix, showMatrix i printTime, przerobienie             *
 *                      funkcji getInfix (teraz getInfixes) tak, aby dobierała końcówki do słów "godzin",              *
 *                      "minut" i "sekund", przerobienie sortowania flagą polską i francuską (teraz sortują            *
 *                      tablicę zmiennych typu char),                                                                  *
 *       - 25.03.2014 - dodanie funkcji searchLinearSaveRes,                                                           *
 *       - 24.03.2014 - dodanie funkcji shiftArray, cmpArrays oraz cmpArraysSaveDif, poprawienie getInfix              *
 *                      dla ujemnych wartości sekund,                                                                  *
 *       - 21.03.2014 - dodanie funkcji sortComb oraz sortGnome, poprawienie algorytmów wyszukiwania (dla              *
 *                      tablic posortowanych malejąco),                                                                *
 *       - 20.03.2014 - pierwsza wersja.                                                                               *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ALGO_H
#define ALGO_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

// Funkcja inicjalizująca bibliotekę.

    void algo_init ();

// Strumień do którego będą przekazywane wyświetlane dane.

    _IO_FILE* algout;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                            1. Definicje struktur danych.                                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Stos:
//
// int value - wartość szczytu stosu,
// Stack* node - wskaźnik na element poprzedzający szczyt stosu.
//
// Aby funkcje do stosu działały poprawnie, należy nowy stos
// zainicjalizować wartością NULL jak na przykładzie poniżej:
// Stack* stos = NULL;

    typedef struct Stack {
        int value;
        struct Stack* node;
    } Stack;

// Komórka kolejki:
//
// Cell* predecessor - wskaźnik na poprzedni element kolejki,
// int value - wartość komórki,
// Cell* successor - wskaźnik na kolejny element kolejki.

    typedef struct Cell {
        struct Cell* predecessor;
        int value;
        struct Cell* successor;
    } Cell;

// Kolejka:
//
// Cell* back - wskaźnik na tył kolejki,
// int length - liczba elementów (długość) kolejki,
// Cell* front - wskaźnik na przód kolejki.
//
// Aby funkcje do kolejki działały poprawnie, należy nową kolejkę
// zainicjalizować funkcją calloc jak na przykładzie poniżej:
// Queue* kolejka = calloc (1, sizeof(Queue));

    typedef struct Queue {
        struct Cell* back;
        int length;
        struct Cell* front;
    } Queue;

// Lista jednokierunkowa:
//
// int value - wartość komórki,
// List* successor - wskaźnik na kolejny element listy.
//
// Aby funkcje do listy działały poprawnie, należy nową listę
// zainicjalizować funkcją calloc, jak na przykładzie poniżej:
// List* lista = calloc (1, sizeof(List));

    typedef struct List {
        int value;
        struct List* successor;
    } List;

// Drzewo binarne:
//
// int value - wartość węzła,
// BinaryTree* right - wskaźnik na prawą gałąź,
// BinaryTree* left - wskaźnik na lewą gałąź.
//
// Aby funkcje do drzewa działały poprawnie, należy nowe drzewo
// zainicjalizować funkcją calloc, jak na przykładzie poniżej:
// BinaryTree* drzewo = calloc (1, sizeof(BinaryTree));

    typedef struct BinaryTree {
        int value;
        struct BinaryTree* right;
        struct BinaryTree* left;
    } BinaryTree;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                2. Funkcje dodatkowe.                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Zamiana dwóch liczb całkowitych.

    void swapInts (int* a, int* b);

// Zamiana dwóch zmiennych znakowych.

    void swapChars (char* a, char* b);

// Zamiana dwóch liczb zmiennoprzecinkowych.

    void swapFloats (float* a, float* b);

// Przeliczenie liczby cyfr w danej liczbie:
//
// int number - dana liczba.
//
// Funkcja wlicza do wyniku znak, jeżeli liczba jest ujemna.

    int intLen (int number);

// Funkcja losuje liczbę całkowitą z podanego przedziału:
//
// int from - początek przedziału,
// int to - koniec przedziału.
//
// Przed użyciem tej funkcji należy ustawić ziarno funkcją srand.
// Funkcja zwraca losową liczbę zmiennoprzecinkową.

    int getRandomInt (int from, int to);

// Funkcja losuje liczbę zmiennoprzecinkową z podanego przedziału:
//
// float from - początek przedziału,
// float to - koniec przedziału.
//
// Przed użyciem tej funkcji należy ustawić ziarno funkcją srand.
// Funkcja zwraca losową liczbę zmiennoprzecinkową.

    float getRandomFloat (float from, float to);

// Funkcja mieszająca tablicę całkowicie losowo:
//
// int* array - tablica, która zostanie przemieszana,
// int length - liczba elementów tej tablicy,
// unsigned int numberOfShuffles - liczba losowych przemieszań.
//
// Przed użyciem tej funkcji należy ustawić ziarno funkcją srand.

    void shuffleNaive (int* array, int length, int numberOfShuffles);

// Funkcja mieszająca tablicę algorytmem Fishera-Yatesa:
//
// int* array - tablica, która zostanie przemieszana,
// int length - liczba elementów tej tablicy.
//
// Przed użyciem tej funkcji należy ustawić ziarno funkcją srand.

    void shuffleFisherYates (int* array, int length);

// Wypelnienie tabliy losowymi elementami:
//
// int* array - tablica, która zostanie wypełniona,
// int length - liczba elementów tej tablicy,
// int from, to - tablica zostanie wypełniona liczbami z zakresu <from, to>.

    void randFill (int* array, int length, int from, int to);

// Wypelnienie tabliy dwuwymiarowej losowymi elementami:
//
// int** matrix - macierz, która zostanie wypełniona,
// int lines - liczba wierszy,
// int columns - liczba kolumn,
// int from, to - macierz zostanie wypełniona liczbami z zakresu <from, to>.

    void randFillMatrix (int** matrix, int lines, int columns, int from, int to);

// Znalezienie losowych składników sumy danej liczby:
//
// int componentsAmount - liczba składników,
// int sum - dana liczba,
// int minValue - minimalna wartość składnika,
// int maxValue - maksymalna wartość składnika.
//
// Funkcja zwraca tablicę liczb całkowitych, której liczba elementów jest
// równa wartości componentsAmount.
// Funkcja zwraca NULL jeżeli liczba składników jest zbyt duża.

    int* getRandomSumComponents (int componentsAmount, int sum, int minValue, int maxValue);

// Zadeklarowanie pamięci dla kolumn tablicy dwuwymiarowej przy użyciu funkcji malloc:
//
// int** matrix - macierz, dla której zostaną utworzone kolumny,
// int lines - liczba wierszy,
// int columnsToCreate - liczba kolumn, które zostaną utworzone.

    void expandMatrix (int** matrix, int lines, int columnsToCreate);

// Przepisanie tablicy dwuwymiarowej do tablicy jednowymiarowej:
//
// int** matrix - macierz, która zostanie przepisana,
// int lines - liczba wierszy,
// int columns - liczba kolumn,
// int* result - utworzona tablica jednowymiarowa,
// char leftToRight - jeżeli 0 - przepisywanie od prawej do lewej, w przeciwnym wypadku odwrotnie,
// char upToDown - jeżeli 0 - przepisywanie od dołu do góry, w przeciwnym wypadku odwrotnie.

    void projectMatrix (int** matrix, int lines, int columns, int* result, char leftToRight, char upToDown);

// Przepisanie tablicy jednowymiarowej do tablicy dwuwymiarowej:
//
// int* array - tablica, która zostanie przepisana,
// int lines - liczba wierszy,
// int columns - liczba kolumn,
// int** result - utworzona macierz,
// char leftToRight - jeżeli 0 - przepisywanie od prawej do lewej, w przeciwnym wypadku odwrotnie,
// char upToDown - jeżeli 0 - przepisywanie od dołu do góry, w przeciwnym wypadku odwrotnie.

    void reprojectMatrix (int* array, int lines, int columns, int** result, char leftToRight, char upToDown);

// Specyficzne (po skosie) przepisanie tablicy dwuwymiarowej do tablicy jednowymiarowej:
//
// int** matrix - macierz, która zostanie przepisana,
// int lines - liczba wierszy,
// int columns - liczba kolumn,
// int* result - utworzona tablica jednowymiarowa.
//
// Elementy są przepisywane jak na poniższym przykładzie:
// 0  1  3  6
// 2  4  7  10
// 5  8  11 13
// 9  12 14 15

    void projectMatrixFromCorner (int** matrix, int lines, int columns, int* result);

// Specyficzne (po skosie) przepisanie tablicy jednowymiarowej do tablicy dwuwymiarowej:
//
// int* array - tablica, która zostanie przepisana,
// int lines - liczba wierszy,
// int columns - liczba kolumn,
// int** result - utworzona macierz.
//
// Elementy są przepisywane jak na poniższym przykładzie:
// [ 0, 1, 3, 6, 2, 4, 7, 10, 5, 8, 11, 13, 9, 12, 14, 15 ]

    void reprojectMatrixToCorner (int* array, int lines, int columns, int** result);

// Wyświetlenie tablicy wg schematu podanego na zajęciach:
//
// int* array - tablica, która zostanie wyświetlona,
// int length - liczba elementów tej tablicy.

    void showArray (int* array, int length);

// Wyświetlenie tablicy dwuwymiarowej wg schematu podanego na zajęciach:
//
// int** matrix - macierz, która zostanie wyświetlona,
// int lines - liczba wierszy,
// int columns - liczba kolumn.
//
// Uwaga! Najlepiej wyświetlane są macierze, których elementy mają równą
// liczbę cyfr.

    void showMatrix (int** matrix, int lines, int columns);

// Wyświetlenie wyników wyszukiwania tekstu:
//
// char* string - tekst, który był przeszukiwany,
// int* result - tablica wyników zwrócona przez funkcję wyszukiwania.

    void showStringSearchResults (char* string, int* result);

// Wyświetlenie dokładnych wyników wyszukiwania tekstu:
//
// char* string - tekst, który był przeszukiwany,
// int* result - tablica wyników zwrócona przez funkcję wyszukiwania,
// int patternLength - liczba znaków we wzorcu.

    void showStringSearchWholeResults (char* string, int* result, int patternLength);

// Wyświetlenie znaków zakodowanych w tablicy wg schematu podanego na zajęciach:
//
// int* array - tablica, która zostanie wyświetlona,
// int length - liczba elementów tej tablicy.

    void showArrayAsChars (int* array, int length);

// Wyświetlenie znaków zakodowanych w tablicy dwuwymiarowej wg schematu podanego na zajęciach:
//
// int** matrix - macierz, która zostanie wyświetlona,
// int lines - liczba wierszy,
// int columns - liczba kolumn.
//
// Uwaga! Najlepiej wyświetlane są macierze, których elementy mają równą
// liczbę cyfr.

    void showMatrixAsChars (int** matrix, int lines, int columns);

// Odwrócenie tablicy (ostatni element jest pierwszym itd.):
//
// int* array - tablica, która zostanie odwrócona,
// int length - liczba elementów danej tablicy.

    void reverseArray (int* array, int length);

// Porównanie dwóch tablic:
//
// int* arrayA, arrayB - tablice liczb całkowitych, które będą porównywane,
// int length - liczba elementów danych tablic.
//
// Funkcja zwraca 1, jeżeli tablice są identyczne, w przeciwnym wypadku - 0.

    int cmpArrays (int* arrayA, int* arrayB, int length);

// Porównanie dwóch tablic i zapisanie indeksów elementów, które się od siebie różnią:
//
// int* arrayA, arrayB - tablice liczb całkowitych, które będą porównywane,
// int length - liczba elementów danych tablic.
//
// Funkcja zwraca tablicę liczb całkowitych. Jej pierwszy element
// to liczba różnic, każdy kolejny - indeks znalezionej różnicy.

    int* cmpArraysSaveDif (int* arrayA, int* arrayB, int length);

// Przeszukiwanie całej tablicy w celu odnalezienia jej maksymalnego / minimalnego elementu:
//
// int* array - tablica liczb całkowitych, która będzie przeszukiwana,
// int length - liczba elementów danej tablicy,
// char getMax - szukanie maksymalnego elementu jeżeli wartość tej zmiennej jest różna od zera.

    int getExtreme (int* array, int length, char getMax);

// Przesunięcie elementów tablicy:
//
// int* array - dana tablica,
// int length - liczba elementów tablicy,
// int value - wartość przesunięcia (jeżeli ujemna - przesunięcie w lewą stronę).
//
// Uwaga! Funkcja nie jest zoptymalizowana; dla wartości przesunięcia
// bliskiej długości tablicy i dla niewielkich wartości ujemnych rozmiar
// stosu jest bliski rozmiarowi tablicy!

    void shiftArray (int* array, int length, int value);

// Funkcja liczy i zwraca czas sortowania danym algorytmem:
//
// void(*sortingMethod)(int, int, char) - dana funkcja sortująca,
// int* array - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.
//
// Funkcja nie liczy czasu działania funkcji sort01 oraz sort012.

    int getSortTime (void(*sortingMethod)(int*, int, char), int* array, int length, char isAscending);

// Dobranie końcówek do słów "godzin", "minut" i "sekund" w zależności od ich liczby:
//
// int hours - liczba godzin,
// int minutes - liczba minut,
// int seconds - liczba sekund.
//
// Funkcja tablicę trzech znaków: końcówki "a" lub "y", jeżeli końcówka nie istnieje - nullbajt.

    char* getInfixes (int hours, int minutes, int seconds);

// Wyświetlenie czasu w formacie "X godzin, Y minut i Z sekund" na podstawie danej liczby
// sekund, dodając do słów "godzin", "minut" oraz "sekund" odpowiednie końcówki:
//
// int seconds - dana liczba sekund.

    void printTime (int seconds);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                           3. Algorytmy sortujące tablicę.                                           *
 *                                                                                                                     *
 *                              Zwracana wartość to czas wykonywania algorytmu w sekundach.                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Sortowanie metodą flagi polskiej:
//
// int* array - dana tablica, w której powinny znajdować się jedynie zera lub jedynki,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sort01 (char* array, int length, char isAscending);

// Sortowanie metodą flagi francuskiej:
//
// int* array - dana tablica, w której powinny znajdować się jedynie zera, jedynki i dwójki,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sort012 (char* array, int length, char isAscending);

// Sortowanie bąbelkowe:
//
// int* array - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortBubble (int* array, int length, char isAscending);

// Sortowanie gnoma:
//
// int* array - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortGnome (int* array, int length, char isAscending);

// Sortowanie przez wstawianie:
//
// int* array - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortInsertion (int* array, int length, char isAscending);

// Sortowanie przez wybranie minimalnej wartości:
//
// int* array - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortSelection (int* array, int length, char isAscending);

// Sortowanie grzebieniowe:
//
// int* array - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortComb (int* array, int length, char isAscending);

// Sortowanie szybkie:
//
// int* array - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortQuick (int* array, int length, char isAscending);

// Właściwe sortowanie szybkie. Wygodniejsza w użyciu jest funkcja powyżej.

    void sortQuickPartially (int* array, int begin, int end, char isAscending);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                        4. Algorytmy przeszukujące tablicę.                                          *
 *                                                                                                                     *
 *                                   Zwracana wartość to indeks szukanego elementu                                     *
 *                                   lub (-1), jeżeli element nie został znaleziony.                                   *
 *                                      Indeks pierwszego elementu tablicy to 0.                                       *
 *                                 Wartością zwracaną przez funkcje przeszukujące ciąg                                 *
 *                                 znaków jest jednowymiarowa tablica indeksów trafień,                                *
 *                                   której pierwszym elementem jest liczba trafień.                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Wyszukiwanie liniowe:
//
// int* array - tablica liczb całkowitych, która będzie przeszukiwana,
// int length - liczba elementów danej tablicy,
// int searchedValue - szukany element tablicy.

    int searchLinear (int* array, int length, int searchedValue);

// Wyszukiwanie liniowe i zapisanie indeksów znalezionych elementów:
//
// int* array - tablica liczb całkowitych, która będzie przeszukiwana,
// int length - liczba elementów danej tablicy,
// int searchedValue - szukany element tablicy.
//
// Funkcja zwraca tablicę liczb całkowitych. Jej pierwszy element
// to liczba trafień, każdy kolejny - indeks znalezionego elementu.

    int* searchLinearSaveRes (int* array, int length, int searchedValue);

// Wyszukiwanie binarne (tylko na posortowanej tablicy):
//
// int* sortedArray - posortowana tablica liczb całkowitych, która będzie przeszukiwana,
// int length - liczba elementów danej tablicy,
// int searchedValue - szukany element tablicy.

    int searchBinary (int* sortedArray, int length, int searchedValue);

// Wyszukiwanie interpolacyjne (tylko na posortowanej tablicy):
//
// int* sortedArray - posortowana tablica liczb całkowitych, która będzie przeszukiwana,
// int length - liczba elementów danej tablicy,
// int searchedValue - szukany element tablicy.

    int searchInterpolating (int* sortedArray, int length, int searchedValue);

// Naiwne wyszukiwanie wzorca w danym tekście:
//
// char* string - tablica znaków, która będzie przeszukiwana,
// int stringLength - liczba elementów danej tablicy znaków,
// char* pattern - tablica znaków, która jest wzorcem,
// int patternLength - liczba znaków we wzorcu.
//
// Funkcja zwraca tablicę liczb całkowitych. Jej pierwszy element
// to liczba trafień, każdy kolejny - indeks pierwszego znaku wzorca
// znalezionego w tekście.

    int* searchStringNaive (char* string, int stringLength, char* pattern, int patternLength);

// Wyszukiwanie wzorca w danym tekście algorytmem Karpa-Rabina:
//
// char* string - tablica znaków, która będzie przeszukiwana,
// int stringLength - liczba elementów danej tablicy znaków,
// char* pattern - tablica znaków, która jest wzorcem,
// int patternLength - liczba znaków we wzorcu,
// int hashingPrime - liczba pierwsza służąca do mieszania, np. 101.
//
// Funkcja zwraca tablicę liczb całkowitych. Jej pierwszy element
// to liczba trafień, każdy kolejny - indeks pierwszego znaku wzorca
// znalezionego w tekście.searchStringKMP

    int* searchStringKR (char* string, int stringLength, char* pattern, int patternLength, int hashingPrime);

// Długość alfabetu dla funkcji searchStringKR.

    #define alphabetLength 256

// Wyszukiwanie wzorca w danym tekście algorytmem Knutha-Morrisa-Pratta:
//
// char* string - tablica znaków, która będzie przeszukiwana,
// int stringLength - liczba elementów danej tablicy znaków,
// char* pattern - tablica znaków, która jest wzorcem,
// int patternLength - liczba znaków we wzorcu.
//
// Funkcja zwraca tablicę liczb całkowitych. Jej pierwszy element
// to liczba trafień, każdy kolejny - indeks pierwszego znaku wzorca
// znalezionego w tekście. Funkcja zwraca tablicę jednoelementową,
// której pierwszy element jest równy (-1) w przypadku, gdy wyznaczenie
// prefiksu się nie powiedzie.

    int* searchStringKMP (char* string, int stringLength, char* pattern, int patternLength);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                            5. Funkcje do struktur danych.                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Położenie elementu na stosie:
//
// int value - wartość elementu,
// Stack** givenStack - dany stos.

    void push (int value, Stack** givenStack);

// Pobranie elementu ze stosu:
//
// Stack** givenStack - dany stos,
// int* value - zmienna, do której zostanie zapisana wartość elementu.
//
// Funkcja zdejmuje (niszczy) element stosu!

    char pop (Stack** givenStack, int* value);

// Pobranie wartości danego elementu stosu bez niszczenia go:
//
// Stack* givenStack - dany stos,
// int depth - dany element (głębokość na jakiej szukamy go w stosie),
// int* result - zmienna, do której zostanie zapisana wartość danego elementu stosu.
//
// Gdy zmienna depth ma wartość mniejszą lub równą 0, szukanym elementem jest szczyt stosu.
// Funkcja zwraca i ustawia wynik na 0, jeżeli dany element nie istnieje, w przeciwnym
// wypadku funkcja zwraca 1.

    char safeGetNodeValue (Stack* givenStack, int depth, int* result);

// Przepisanie stosu do tablicy jednowymiarowej:
//
// Stack** givenStack - dany stos,
// int* result - tablica, do której zostanie przepisany stos.
//
// Uwaga! Funkcja niszczy zawartość stosu.

    void projectStack (Stack** givenStack, int* result);

// Przepisanie stosu do tablicy jednowymiarowej bez niszczenia stosu:
//
// Stack* givenStack - dany stos,
// int* result - tablica, do której zostanie przepisany stos.

    void safeProjectStack (Stack* givenStack, int* result);

// Przepisanie tablicy jednowymiarowej do stosu:
//
// int* givenArray - dana tablica,
// int length - liczba elementów tej tablicy,
// Stack** result - stos, na który zostaną położone elementy tablicy.
//
// Funkcja przepisuje tablicę od ostatniego elementu do pierwszego.

    void reprojectStack (int* givenArray, int length, Stack** result);

// Znalezienie wysokości stosu:
//
// Stack* givenStack - dany stos.
//
// Funkcja lepsza od poniższej, nie przepisuje dwukrotnie stosu.

    int getStackHeight (Stack* givenStack);

// Znalezienie wysokości stosu:
//
// Stack** givenStack - dany stos.
//
// Funkcja zwraca liczbę elementów stosu, stos nie jest niszczony, ale
// jest dwukrotnie przepisywany.

    int getStackHeightWorse (Stack** givenStack);

// Odwrócenie stosu:
//
// Stack** givenStack - dany stos.

    void reverseStack (Stack** givenStack);

// Usunięcie stosu (zwolnienie pamięci przez niego zajmowanej):
//
// Stack** givenStack - dany stos.

    void deleteStack (Stack** givenStack);

// Dodanie komórki na tył kolejki:
//
// int value - wartość nowej komórki,
// Queue** givenQueue - dana kolejka.
//
// Funkcja zwraca wskaźnik na nowo dodany element kolejki.

    Cell* enQueue (int value, Queue** givenQueue);

// Pobranie komórki z przodu kolejki:
//
// Queue** givenQueue - dana kolejka,
// int* value - zmienna, do której zostanie zapisana wartość komórki.
//
// Funkcja zwraca 0 w przypadku niepowodzenia lub 1 w przeciwnym wypadku.
// Funkcja usuwa pobraną komórkę!

    char deQueue (Queue** givenQueue, int* value);

// Znalezienie liczby komórek w kolejce:
//
// Queue* givenQueue - dana kolejka.
//
// Funkcja zwraca liczbę komórek.

    int getQueueLength (Queue* givenQueue);

// Przepisanie kolejki do tablicy jednowymiarowej bez niszczenia kolejki:
//
// Queue* givenQueue - dana kolejka,
// int* result - tablica, do której zostanie przepisana kolejka.

    void projectQueue (Queue* givenQueue, int* result);

// Przepisanie tablicy jednowymiarowej do kolejki:
//
// int* givenArray - dana tablica,
// int length - liczba elementów tej tablicy,
// Queue* givenQueue - kolejka, do której zostaną przepisane elementy tablicy.

    void reprojectQueue (int* givenArray, int length, Queue** result);

// Dodanie nowej komórki do kolejki w odniesieniu do innej komórki:
//
// int value - wartość nowej komórki,
// Queue* givenQueue - dana kolejka,
// Cell* referenceCell - komórka, w odniesieniu do której zostanie dodana nowa komórka,
// char direction - jeżeli zmienna ma wartość 'r', komórka zostanie dodana jako następnik,
//                  jeżeli zmienna ma wartość 'l', komórka zostanie dodana jako poprzednik.
//
// Funkcja nie dodaje komórek na początku i końcu kolejki.
// Do tego służą funkcje addCellToFront oraz enQueue.
// Funkcja zwraca 1, jeżeli operacja się powiedzie, w przeciwnym wypadku funkcja zwraca 0.

    char addCell (int value, Queue* givenQueue, Cell* referenceCell, char direction);

// Dodanie komórki z przodu kolejki:
//
// int value - wartość nowej komórki,
// Queue** givenQueue - dana kolejka.
//
// Funkcja zwraca wskaźnik na nową komórkę kolejki.

    Cell* addCellToFront (int value, Queue** givenQueue);

// Usunięcie kolejki (zwolnienie pamięci przez nią zajmowanej):
//
// Queue** givenQueue - dana kolejka.

    void deleteQueue (Queue** givenQueue);

// Dodanie elementu do listy:
//
// int value - wartość dodawanego elementu,
// List* givenList - wskaźnik na pierwszy element danej listy,
// int predecessorIndex - indeks elementu poprzedzającego dodawany element.
//
// Funkcja zwraca wskaźnik na nowo dodany element listy.

    List* insertToList (int value, List* givenList, int predecessorIndex);

// Usunicie elementu z listy:
//
// List** givenList - wskaźnik na pierwszy element danej listy,
// int index - indeks elementu do usunięcia.

    char deleteFromList (List* givenList, int index);

// Zmiana wartości danego elementu listy:
//
// int value - nowa wartość elementu,
// List* givenList - wskaźnik na pierwszy element danej listy,
// int index - indeks elementu, którego wartość zostanie zmieniona.
//
// Funkcja zwraca poprzednią wartość elementu.

    int setListValue (int value, List* givenList, int index);

// Pobranie wartości danego elementu listy:
//
// List* givenList - wskaźnik na pierwszy element danej listy,
// int index - indeks elementu, którego wartość zostanie pobrana.
//
// Funkcja zwraca wartość danego elementu listy.

    int getListValue (List* givenList, int index);

// Zmiana liczby elementów listy:
//
// List* givenList - wskaźnik na pierwszy element danej listy,
// int newLength - liczba elementów listy.
//
// Funkcja nadaje nowym elementom wartość 0.
// Funkcja zwraca 0, jeżeli liczba elementów listy nie została zmieniona,
// 1 jeżeli liczba ta została zwiększona lub (-1), jeżeli liczba elementów
// listy została zmniejszona.

    char setListLength (List* givenList, int newLength);

// Znalezienie liczby elementów listy:
//
// List* givenList - wskaźnik na pierwszy element danej listy,
//
// Funkcja zwraca liczbę elementów danej listy.

    int getListLength (List* givenList);

// Przepisanie listy do tablicy jednowymiarowej bez niszczenia listy:
//
// List* givenList - wskaźnik na pierwszy element danej listy,
// int* result - tablica, do której zostanie przepisana lista.
//
// Funkcja zwraca 0, jeżeli lista nie zawiera elementów i nie może
// zostać przepisana, w przeciwnym wypadku funkcja zwraca 1.

    char projectList (List* givenList, int* result);

// Przepisanie tablicy jednowymiarowej do listy:
//
// int* givenArray - tablica, która zostanie przepisana,
// int length - liczba elementów tablicy,
// List* result - wskaźnik na pierwszy element danej listy.
//
// Funkcja zwraca 0, jeżeli liczba elementów tablicy jest
// mniejsza lub równa zeru, w przeciwnym wypadku funkcja zwraca 1.
// Elementy są dopisywane do końca listy; nie jest to najbardziej
// optymalna metoda.

    char reprojectList (int* givenArray, int length, List* result);

// Dodanie węzła do danego drzewa binarnego:
//
// int value - wartość nowego węzła,
// BinaryTree** givenTree - dane drzewo.
//
// Funkcja zwraca wskaźnik na nowo utworzony węzeł.

    BinaryTree* insert (int value, BinaryTree** givenTree);

// Znalezienie rozmiaru drzewa (liczby jego elementów):
//
// BinaryTree* givenTree - dane drzewo.
//
// Funkcja zwraca liczbę elementów danego drzewa.

    int getTreeSize (BinaryTree* givenTree);

// Wyszukiwanie węzła o danej wartości w drzewie binarnym:
//
// BinaryTree** givenTree - dane drzewo,
// int searchedValue - wyszukiwana wartość.
//
// Funkcja zwraca wskaźnik na znaleziony węzeł.

    BinaryTree* searchBinaryTree (BinaryTree** givenTree, int searchedValue);

// Przepisanie drzewa binarnego do tablicy jednowymiarowej:
//
// BinaryTree* givenTree - dane drzewo,
// int* result - tablica jednowymiarowa, do której zostanie zapisany wynik (liczba elementów koniecznie
//               równa liczbie elementów drzewa),
// int* counter - wyzerowana (!) zmienna służaca za licznik elementów,
// char order - porządek, w jakim przepisywane jest drzewo, jeżeli zmienna jest równa
//              -1 - drzewo przepisywane od góry, najpierw przepisywane gałęzie po lewej (pre order),
//              0 - elementy przepisywane w kolejności rosnącej (in order),
//              1 - drzewo jest przepisywane od dołu (post order).
//
// Funkcja zwraca 0, jeżeli w drzewie nie ma żadnych elementów, w przeciwnym wypadku funkcja zwraca 1.

    char projectTree (BinaryTree* givenTree, int* result, int* counter, char order);

// Dopisanie tablicy jednowymiarowej do drzewa:
//
// int* givenArray - dana tablica,
// int length - liczba elementów tablicy,
// BinaryTree*** result - utworzone drzewo.

    void reprojectTree (int* givenArray, int length, BinaryTree** result);

// Usunięcie drzewa (zwolnienie pamięci przez nie zajmowanej):
//
// BinaryTree* givenTree - dane drzewo.

    void deleteTree (BinaryTree** givenTree);

#endif

// Źródła:
//
// http://en.wikibooks.org/wiki/Algorithm_Implementation/Sorting
// http://en.wikibooks.org/wiki/Algorithm_Implementation/Search
// http://algorytmy.blox.pl/2008/03/Wyszukiwanie-interpolacyjne.html
// http://www.geeksforgeeks.org/searching-for-patterns-set-3-rabin-karp-algorithm/
// http://kolos.math.uni.lodz.pl/~archive/Algorytmy%20i%20struktury%20danych%202%20%28Zaawansowane%20algorytmy%29/Projekt%204%20-%20Wzorzec/wyk12.pdf
// http://rosettacode.org/wiki/
// http://stackoverflow.com/questions/6579456/implementation-of-queue-using-pointers-segmentation-error
// http://en.wikipedia.org/wiki/Abstract_data_structure
// https://en.wikipedia.org/wiki/List_%28abstract_data_type%29
// http://en.wikipedia.org/wiki/List_of_data_structures#Binary_trees
// http://www.thegeekstuff.com/2013/02/c-binary-tree/
