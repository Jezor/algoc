# Algorytmy i struktury danych. #

## 1. Definicje struktur danych: ##
 * Stack - stos, 
 * Cell - komórka kolejki, 
 * Queue - kolejka,
 * List - lista jednokierunkowa, 
 * BinaryTree - drzewo binarne.

## 2. Funkcje dodatkowe: ##
 * swapInts - zamiana dwóch liczb całkowitych, 
 * swapChars - zamiana dwóch zmiennych znakowych,
 * intLen - przeliczenie liczby znaków w danej liczbie,
 * getRandomInt - funkcja losuje liczbę całkowitą z podanego przedziału, 
 * getRandomFloat - funkcja losuje liczbę zmiennoprzecinkową z podanego przedziału,
 * shuffleNaive - funkcja mieszająca tablicę całkowicie losowo,
 * shuffleFisherYates - funkcja mieszająca tablicę algorytmem Fishera-Yatesa,
 * randFill - wypełnienie tablicy losowymi liczbami, 
 * randFillMatrix - wypełnienie tablicy dwuwymiarowej losowymi liczbami, 
 * getRandomSumComponents - znalezienie losowych składników sumy danej liczby, 
 * expandMatrix - zadeklarowanie pamięci dla kolumn tablicy dwuwymiarowej, 
 * projectMatrix - przepisanie tablicy dwuwymiarowej do tablicy jednowymiarowej, 
 * reprojectMatrix - przepisanie tablicy jednowymiarowej do tablicy dwuwymiarowej, 
 * projectMatrixFromCorner - specyficzne przepisanie macierzy do tablicy,
 * reprojectMatrixFromCorner - specyficzne przepisanie tablicy do macierzy,
 * showArray - wyświetlenie tablicy, 
 * showMatrix - wyświetlenie tablicy dwuwymiarowej,
 * showStringSearchResults - wyświetlenie wyników wyszukiwania tekstu, 
 * showStringSearchWholeResults - wyświetlenie dokładnych wyników wyszukiwania tekstu, 
 * reverseArray - odwrócenie tablicy,
 * cmpArrays - porównanie dwóch tablic,
 * cmpArraysSaveDif - porównanie dwóch tablic i zapisanie różnic,
 * getExtreme - znalezienie ekstremum tablicy, 
 * shiftArray - przesunięcie elementów tablicy,
 * getSortTime - funkcja liczy i zwraca czas sortowania danym algorytmem,
 * getInfixes - dobranie końcówki do słów "godzin", "minut" i "sekund",
 * printTime - wyświetlenie czasu danego w sekundach.

## 3. Algorytmy sortujące: ##
 * sort01 - flaga polska,
 * sort012 - flaga francuska,
 * sortBubble - bąbelkowe, 
 * sortGnome - gnoma,
 * sortInsertion - przez wstawianie, 
 * sortSelection - przez wybranie minimalnej wartości, 
 * sortComb - sortowanie grzebieniowe, 
 * sortQuick - sortowanie szybkie. 

## 4. Algorytmy wyszukiwania: ##
 * searchLinear - liniowe, 
 * searchLinearSaveRes - liniowe z zapisaniem trafień, 
 * searchBinary - binarne, 
 * searchInterpolating - interpolacyjne, 
 * searchStringNaive - naiwne wyszukiwanie tekstu, 
 * searchStringKR - wyszukiwanie tekstu algorytmem Karpa-Rabina, 
 * searchStringKMP - Knutha-Morrisa-Pratta.

## 5. Funkcje do struktur danych: ##
 * push - położenie elementu na stosie,
 * pop - pobranie elementu ze stosu, 
 * safeGetNodeValue - pobranie wartości danego elementu stosu bez niszczenia go, 
 * projectStack - przepisanie stosu do tablicy jednowymiarowej (stos jest niszczony),
 * safeProjectStack - przepisanie stosu do tablicy jednowymiarowej bez niszczenia stosu, 
 * reprojectStack - przepisanie tablicy jednowymiarowej do stosu,
 * getStackHeight - znalezienie wysokości stosu, 
 * getStackHeightWorse - to co wyżej, tylko sposobem z laborków, 
 * reverseStack - odwrócenie stosu,
 * deleteStack - usunięcie stosu (zwolnienie pamięci przez niego zajmowanej),
 * enQueue - dodanie komórki na tył kolejki, 
 * deQueue - pobranie komórki z przodu kolejki,
 * getQueueLength - znalezienie liczby komórek w kolejce,
 * projectQueue - przepisanie kolejki do tablicy jednowymiarowej,
 * reprojectQueue - przepisanie tablicy jednowymiarowej do kolejki,
 * addCell - dodanie nowej komórki do kolejki w odniesieniu do innej komórki,
 * addCellToFront - dodanie komórki z przodu kolejki,
 * deleteQueue - usunięcie kolejki (zwolnienie pamięci przez nią zajmowanej),
 * insertToList - dodanie elementu do listy, 
 * deleteFromList - usunicie elementu z listy, 
 * setListValue - zmiana wartości danego elementu listy, 
 * getListValue - pobranie wartości danego elementu listy, 
 * setListLength - zmiana liczby elementów listy,
 * getListLength - znalezienie liczby elementów listy, 
 * projectList - przepisanie listy do tablicy jednowymiarowej bez niszczenia listy,
 * reprojectList - przepisanie tablicy jednowymiarowej do listy, 
 * insert - dodanie węzła do danego drzewa binarnego,
 * getTreeSize - znalezienie rozmiarów drzewa (liczby jego elementów), 
 * searchBinaryTree - wyszukiwanie węzła o danej wartości w drzewie binarnym,
 * projectTree - przepisanie drzewa binarnego do tablicy jednowymiarowej,
 * reprojectTree - dopisanie tablicy jednowymiarowej do drzewa,
 * deleteTree - usunięcie drzewa (zwolnienie pamięci przez nie zajmowanej).

## Źródła: ##
 * http://en.wikibooks.org/wiki/Algorithm_Implementation/Sorting
 * http://en.wikibooks.org/wiki/Algorithm_Implementation/Search
 * http://algorytmy.blox.pl/2008/03/Wyszukiwanie-interpolacyjne.html
 * http://www.geeksforgeeks.org/searching-for-patterns-set-3-rabin-karp-algorithm/
 * http://kolos.math.uni.lodz.pl/~archive/Algorytmy%20i%20struktury%20danych%202%20%28Zaawansowane%20algorytmy%29/Projekt%204%20-%20Wzorzec/wyk12.pdf
 * http://rosettacode.org/wiki/
 * http://stackoverflow.com/questions/6579456/implementation-of-queue-using-pointers-segmentation-error
 * http://en.wikipedia.org/wiki/Abstract_data_structure
 * https://en.wikipedia.org/wiki/List_%28abstract_data_type%29
 * http://en.wikipedia.org/wiki/List_of_data_structures#Binary_trees
 * http://www.thegeekstuff.com/2013/02/c-binary-tree/

# Autor: Paweł Fiuk #
 * [e-mail](mailto:pawelfiu@gmail.com)
 * [Facebook](https://www.facebook.com/pawelfiukczy)
 * [LinkedIn](https://www.linkedin.com/in/paweł-fiuk-b370b9100)

**Projekt nie jest przez mnie rozwijany. Ostatnie poprawki wprowadziłem w lutym 2015.**

**Mimo tego na bieżąco będę przeglądał wszystkie zmiany / propozycje.**

**Jeżeli chcesz pracować nad rozwojem projektu i potrzebujesz dodatkowych uprawnień do repozytorium - napisz do mnie maila.**

**Udzielam też korepetycji z programowania, moje ogłoszenie można znaleźć [tutaj](http://www.e-korepetycje.net/jezor/programowanie).**