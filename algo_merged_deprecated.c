/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                            Algorytmy i struktury danych.                                            *
 *                                                     07.07.2014                                                      *
 *                                                                                                                     *
 * 1. Definicje struktur danych:                                                                                       *
 *       - Point - punkt,                                                                                              *
 *       - Stack - stos,                                                                                               *
 *       - Cell - komórka kolejki,                                                                                     *
 *       - Queue - kolejka,                                                                                            *
 *       - List - lista jednokierunkowa,                                                                               *
 *       - BinaryTree - drzewo binarne.                                                                                *
 *                                                                                                                     *
 * 2. Funkcje dodatkowe:                                                                                               *
 *       - swapInts - zamiana dwóch liczb całkowitych,                                                                 *
 *       - swapChars - zamiana dwóch zmiennych znakowych,                                                              *
 *       - intLen - przeliczenie liczby znaków w danej liczbie,                                                        *
 *       - getRandomInt - funkcja losuje liczbę całkowitą z podanego przedziału,                                       *
 *       - getRandomFloat - funkcja losuje liczbę zmiennoprzecinkową z podanego przedziału,                            *
 *       - shuffleNaive - funkcja mieszająca tablicę całkowicie losowo,                                                *
 *       - shuffleFisherYates - funkcja mieszająca tablicę algorytmem Fishera-Yatesa,                                  *
 *       - randFill - wypełnienie tablicy losowymi liczbami,                                                           *
 *       - randFillMatrix - wypełnienie tablicy dwuwymiarowej losowymi liczbami,                                       *
 *       - getRandomSumComponents - znalezienie losowych składników sumy danej liczby,                                 *
 *       - expandMatrix - zadeklarowanie pamięci dla kolumn tablicy dwuwymiarowej,                                     *
 *       - projectMatrix - przepisanie tablicy dwuwymiarowej do tablicy jednowymiarowej,                               *
 *       - reprojectMatrix - przepisanie tablicy jednowymiarowej do tablicy dwuwymiarowej,                             *
 *       - projectMatrixFromCorner - specyficzne przepisanie macierzy do tablicy,                                      *
 *       - reprojectMatrixFromCorner - specyficzne przepisanie tablicy do macierzy,                                    *
 *       - showArray - wyświetlenie tablicy,                                                                           *
 *       - showMatrix - wyświetlenie tablicy dwuwymiarowej,                                                            *
 *       - showStringSearchResults - wyświetlenie wyników wyszukiwania tekstu,                                         *
 *       - showStringSearchWholeResults - wyświetlenie dokładnych wyników wyszukiwania tekstu,                         *
 *       - reverseArray - odwrócenie tablicy,                                                                          *
 *       - cmpArrays - porównanie dwóch tablic,                                                                        *
 *       - cmpArraysSaveDif - porównanie dwóch tablic i zapisanie różnic,                                              *
 *       - getExtreme - znalezienie ekstremum tablicy,                                                                 *
 *       - shiftArray - przesunięcie elementów tablicy,                                                                *
 *       - getSortTime - funkcja liczy i zwraca czas sortowania danym algorytmem,                                      *
 *       - getInfixes - dobranie końcówki do słów "godzin", "minut" i "sekund",                                        *
 *       - printTime - wyświetlenie czasu danego w sekundach.                                                          *
 *                                                                                                                     *
 * 3. Algorytmy sortujące:                                                                                             *
 *       - sort01 - flaga polska,                                                                                      *
 *       - sort012 - flaga francuska,                                                                                  *
 *       - sortBubble - bąbelkowe,                                                                                     *
 *       - sortGnome - gnoma,                                                                                          *
 *       - sortInsertion - przez wstawianie,                                                                           *
 *       - sortSelection - przez wybranie minimalnej wartości,                                                         *
 *       - sortComb - sortowanie grzebieniowe,                                                                         *
 *       - sortQuick - sortowanie szybkie.                                                                             *
 *                                                                                                                     *
 * 4. Algorytmy wyszukiwania:                                                                                          *
 *       - searchLinear - liniowe,                                                                                     *
 *       - searchLinearSaveRes - liniowe z zapisaniem trafień,                                                         *
 *       - searchBinary - binarne,                                                                                     *
 *       - searchInterpolating - interpolacyjne,                                                                       *
 *       - searchStringNaive - naiwne wyszukiwanie tekstu,                                                             *
 *       - searchStringKR - wyszukiwanie tekstu algorytmem Karpa-Rabina,                                               *
 *       - searchStringKMP - Knutha-Morrisa-Pratta.                                                                    *
 *                                                                                                                     *
 * 5. Funkcje do struktur danych:                                                                                      *
 *       - getDistanceBetweenPoints - funkcja oblicza odległość pomiędzy dwoma punktami,                               *
 *       - getRandomPointInCircle - funkcja wyznacza losowy punkt zawarty w danym okręgu,                              *
 *       - push - położenie elementu na stosie,                                                                        *
 *       - pop - pobranie elementu ze stosu,                                                                           *
 *       - safeGetNodeValue - pobranie wartości danego elementu stosu bez niszczenia go,                               *
 *       - projectStack - przepisanie stosu do tablicy jednowymiarowej (stos jest niszczony),                          *
 *       - safeProjectStack - przepisanie stosu do tablicy jednowymiarowej bez niszczenia stosu,                       *
 *       - reprojectStack - przepisanie tablicy jednowymiarowej do stosu,                                              *
 *       - getStackHeight - znalezienie wysokości stosu,                                                               *
 *       - getStackHeightWorse - to co wyżej, tylko sposobem z laborków,                                               *
 *       - reverseStack - odwrócenie stosu,                                                                            *
 *       - deleteStack - usunięcie stosu (zwolnienie pamięci przez niego zajmowanej),                                  *
 *       - enQueue - dodanie komórki na tył kolejki,                                                                   *
 *       - deQueue - pobranie komórki z przodu kolejki,                                                                *
 *       - getQueueLength - znalezienie liczby komórek w kolejce,                                                      *
 *       - projectQueue - przepisanie kolejki do tablicy jednowymiarowej,                                              *
 *       - reprojectQueue - przepisanie tablicy jednowymiarowej do kolejki,                                            *
 *       - addCell - dodanie nowej komórki do kolejki w odniesieniu do innej komórki,                                  *
 *       - addCellToFront - dodanie komórki z przodu kolejki,                                                          *
 *       - deleteQueue - usunięcie kolejki (zwolnienie pamięci przez nią zajmowanej),                                  *
 *       - insertToList - dodanie elementu do listy,                                                                   *
 *       - deleteFromList - usunicie elementu z listy,                                                                 *
 *       - setListValue - zmiana wartości danego elementu listy,                                                       *
 *       - getListValue - pobranie wartości danego elementu listy,                                                     *
 *       - setListLength - zmiana liczby elementów listy,                                                              *
 *       - getListLength - znalezienie liczby elementów listy,                                                         *
 *       - projectList - przepisanie listy do tablicy jednowymiarowej bez niszczenia listy,                            *
 *       - reprojectList - przepisanie tablicy jednowymiarowej do listy,                                               *
 *       - insert - dodanie węzła do danego drzewa binarnego,                                                          *
 *       - getTreeSize - znalezienie rozmiarów drzewa (liczby jego elementów),                                         *
 *       - searchBinaryTree - wyszukiwanie węzła o danej wartości w drzewie binarnym,                                  *
 *       - projectTree - przepisanie drzewa binarnego do tablicy jednowymiarowej,                                      *
 *       - reprojectTree - dopisanie tablicy jednowymiarowej do drzewa,                                                *
 *       - deleteTree - usunięcie drzewa (zwolnienie pamięci przez nie zajmowanej).                                    *
 *                                                                                                                     *
 *    Lista zmian:                                                                                                     *
 *       - 07.07.2014 - dodanie funkcji getRandomSumComponents, poprawienie obsługi błędów (typ ErrorName              *
 *                      zamiast ID błędu),                                                                             *
 *       - 29.06.2014 - dodanie funkcji shuffleFisherYates (makro: xaShuffleArray) oraz shuffleNaive,                  *
 *                      zrefaktoryzowanie nazw struktur danych (od teraz zaczynają się wielką literą), dodanie         *
 *                      w warunkach niektórych funkcji struktur danych zapytanie o NULL (zamiast zwykłego              *
 *                      przyrównania do zera) ze względu na to, że NULL nie zawsze jest równy zeru, poprawienie        *
 *                      funkcji deleteTree, funkcje enQueue, addCellToFront, insertToList oraz insert zwracają         *
 *                      teraz wskaźnik na nowo dodane do struktury elementy,                                           *
 *       - 01.06.2014 - poprawiona została funkcja getRandomInt, dodana struktura Point oraz funkcje                   *
 *                      getDistanceBetweenPoints i getRandomPointInCircle,                                             *
 *       - 30.05.2014 - dodanie obsługi błędów (funkcja errorHandler) oraz funkcji reprojectList, getRandomInt,        *
 *                      getRandomFloat i getSortTime, większość funkcji sortujących nie zwraca już swojego             *
 *                      czasu wykonywania,                                                                             *
 *       - 29.05.2014 - dodanie listy jednokierunkowej oraz funkcji insertToList, deleteFromList, setListValue,        *
 *                      setListValue, setListLength, getListLength, projectList,                                       *
 *       - 13.04.2014 - poprawienie funkcji shiftArray, dodanie pola długości dla kolejki (dzięki temu podczas         *
 *                      mierzenia jej długości nie trzeba zliczać wszystkich elementów - funkcje do kolejek            *
 *                      zostały zmienione), dodanie funkcji projectQueue, reprojectQueue, deleteStack,                 *
 *                      deleteQueue, dodanie makr dla łatwiejszego posługiwania się wybranymi funkcjami (nazwy         *
 *                      makr zaczynają się od "xa"), dodanie definicji drzewa binarnego, dodanie funkcji insert,       *
 *                      getTreeSize, searchBinaryTree, projectTree, reprojectTree oraz deleteTree,                     *
 *       - 11.04.2014 - dodanie definicji stosu i kolejki, dodanie funkcji push, pop, safeGetNodeValue,                *
 *                      projectStack, safeProjectStack, reprojectStack, getStackHeight, reverseStack, enQueue,         *
 *                      deQueue, getQueueLength, addCell oraz addCellToFront, poprawienie komentarzy do                *
 *                      wszystkich funkcji (od teraz przy nazwach zmiennych są też ich typy).                          *
 *       - 03.04.2014 - dodanie stałej alphabetLength, dodanie funkcji searchStringKMP, poprawienie funkcji            *
 *                      cmpArraysSaveDif oraz searchLinearSaveRes tak, aby zapisywały wyniki tylko do jednej           *
 *                      tablicy.                                                                                       *
 *       - 02.04.2014 - dodanie funkcji searchStringNaive, searchStringKR, showStringSearchResults oraz                *
 *                      showStringSearchWholeResults,                                                                  *
 *       - 31.03.2014 - dodanie funkcji reverseArray,                                                                  *
 *       - 30.03.2014 - dodanie funkcji swapChars, intLen, projectMatrixFromCorner i reprojectMatrixFromCorner,        *
 *                      przerobienie funkcji showArray oraz showMatrix tak, aby wyświetlały do 80 znaków na            *
 *                      linię (ze względu na problem z wyświetlaniem dużych liczb),                                    *
 *       - 28.03.2014 - usunięcie funkcji randDoubleFill i randTripleFill, dodanie funkcji randFillMatrix,             *
 *                      createMatrix, projectMatrix, reprojectMatrix, showMatrix i printTime, przerobienie             *
 *                      funkcji getInfix (teraz getInfixes) tak, aby dobierała końcówki do słów "godzin",              *
 *                      "minut" i "sekund", przerobienie sortowania flagą polską i francuską (teraz sortują            *
 *                      tablicę zmiennych typu char),                                                                  *
 *       - 25.03.2014 - dodanie funkcji searchLinearSaveRes,                                                           *
 *       - 24.03.2014 - dodanie funkcji shiftArray, cmpArrays oraz cmpArraysSaveDif, poprawienie getInfix              *
 *                      dla ujemnych wartości sekund,                                                                  *
 *       - 21.03.2014 - dodanie funkcji sortComb oraz sortGnome, poprawienie algorytmów wyszukiwania (dla              *
 *                      tablic posortowanych malejąco),                                                                *
 *       - 20.03.2014 - pierwsza wersja.                                                                               *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ALGO_H
#define ALGO_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#define alphabetLength 256

// Obsługa błędów:
//
// char* functionName - nazwa funkcji, w której występuje błąd,
//                      jeżeli NULL, nazwa nie jest wyświetlana,
// char isInternal - 0, jeżeli błąd nie dotyczy działania funkcji
//                   a sposobu, w jaki została wywołana,
// int errorID - identyfikator błędu.
//
// Funkcja wyświetla błędy na standardowym wyjściu (stderr).

    typedef enum ErrorName {
        VALUE_EXCEEDS_RANGE,
        INCORRECT_ARRAY_LENGTH,
        INCORRECT_MATRIX_SIZE,
        INCORRECT_INDEX,
        NOT_SPECIFIED
    } ErrorName;

    void errorHandler (char* functionName, char isInternal, ErrorName name) {
        time_t rawtime;
        struct tm* timeinfo;
        time(&rawtime);
        timeinfo = localtime(&rawtime);

        if (isInternal && functionName)
            fprintf(stderr, "%sInternal function error in %s: ", asctime(timeinfo), functionName);
        if (isInternal && (!functionName))
            fprintf(stderr, "%sInternal function error: ", asctime(timeinfo));
        if ((!isInternal) && functionName)
            fprintf(stderr, "%sFunction casting error in %s: ", asctime(timeinfo), functionName);
        if ((!isInternal) && (!functionName))
            fprintf(stderr, "%sError: ", asctime(timeinfo));

        switch (name) {
            case VALUE_EXCEEDS_RANGE:
                fprintf(stderr, "value exceeds range!\n");
                break;

            case INCORRECT_ARRAY_LENGTH:
                fprintf(stderr, "incorrect array length!\n");
                break;

            case INCORRECT_MATRIX_SIZE:
                fprintf(stderr, "incorrect matrix size!\n");
                break;

            case INCORRECT_INDEX:
                fprintf(stderr, "incorrect index!\n");
                break;

            default:
                fprintf(stderr, "not specified.\n");
                break;
        }
    }

// Definicje makr:

#define xaRandFill(array) randFill(array, ((sizeof(array)) / (sizeof(int))), 10, 99)
#define xaShowArray(array) showArray(array, ((sizeof(array)) / (sizeof(int))))
#define xaCmpArrays(arrayA, arrayB) cmpArrays(arrayA, arrayB, ((sizeof(arrayA)) / (sizeof(int))))
#define xaShuffleArray(array) shuffleFisherYates(array, ((sizeof(array)) / (sizeof(int))))

#define xaSort01(array) sort01(array, ((sizeof(array)) / (sizeof(int))), 1)
#define xaSort012(array) sort012(array, ((sizeof(array)) / (sizeof(int))), 1)
#define xaSortBubble(array) sortBubble(array, ((sizeof(array)) / (sizeof(int))), 1)
#define xaSortGnome(array) sortGnome(array, ((sizeof(array)) / (sizeof(int))), 1)
#define xaSortInsertion(array) sortInsertion(array, ((sizeof(array)) / (sizeof(int))), 1)
#define xaSortSelection(array) sortSelection(array, ((sizeof(array)) / (sizeof(int))), 1)
#define xaSortComb(array) sortComb(array, ((sizeof(array)) / (sizeof(int))), 1)
#define xaSortQuick(array) sortQuick(array, ((sizeof(array)) / (sizeof(int))), 1)

#define xaSearchLinear(array, searchedValue) searchLinear(array, ((sizeof(array)) / (sizeof(int))), searchedValue)
#define xaSearchLinearSaveRes(array, searchedValue) searchLinearSaveRes(array, ((sizeof(array)) / (sizeof(int))), searchedValue)
#define xaSearchBinary(array, searchedValue) searchBinary(array, ((sizeof(array)) / (sizeof(int))), searchedValue)
#define xaSearchInterpolating(array, searchedValue) searchInterpolating(array, ((sizeof(array)) / (sizeof(int))), searchedValue)
#define xaSearchStringNaive(string, pattern) searchStringNaive(string, sizeof(string), pattern, sizeof(pattern))
#define xaSearchStringKR(string, pattern) searchStringKR(string, sizeof(string), pattern, sizeof(pattern), 101)
#define xaSearchStringKMP(string, pattern) searchStringKMP(string, sizeof(string), pattern, sizeof(pattern))

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                            1. Definicje struktur danych.                                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Punkt:
//
// float x - współrzędna pozioma,
// float y - współrzędna pionowa.

    typedef struct Point {
        float x;
        float y;
    } Point;

// Stos:
//
// int value - wartość szczytu stosu,
// Stack* node - wskaźnik na element poprzedzający szczyt stosu.
//
// Aby funkcje do stosu działały poprawnie, należy nowy stos
// zainicjalizować wartością NULL jak na przykładzie poniżej:
// Stack* stos = NULL;

    typedef struct Stack {
        int value;
        struct Stack* node;
    } Stack;

// Komórka kolejki:
//
// Cell* predecessor - wskaźnik na poprzedni element kolejki,
// int value - wartość komórki,
// Cell* successor - wskaźnik na kolejny element kolejki.

    typedef struct Cell {
        struct Cell* predecessor;
        int value;
        struct Cell* successor;
    } Cell;

// Kolejka:
//
// Cell* back - wskaźnik na tył kolejki,
// int length - liczba elementów (długość) kolejki,
// Cell* front - wskaźnik na przód kolejki.
//
// Aby funkcje do kolejki działały poprawnie, należy nową kolejkę
// zainicjalizować funkcją calloc jak na przykładzie poniżej:
// Queue* kolejka = calloc (1, sizeof(Queue));

    typedef struct Queue {
        struct Cell* back;
        int length;
        struct Cell* front;
    } Queue;

// Lista jednokierunkowa:
//
// int value - wartość komórki,
// List* successor - wskaźnik na kolejny element listy.
//
// Aby funkcje do listy działały poprawnie, należy nową listę
// zainicjalizować funkcją calloc, jak na przykładzie poniżej:
// List* lista = calloc (1, sizeof(List));

    typedef struct List {
        int value;
        struct List* successor;
    } List;

// Drzewo binarne:
//
// int value - wartość węzła,
// BinaryTree* right - wskaźnik na prawą gałąź,
// BinaryTree* left - wskaźnik na lewą gałąź.
//
// Aby funkcje do drzewa działały poprawnie, należy nowe drzewo
// zainicjalizować funkcją calloc, jak na przykładzie poniżej:
// BinaryTree* drzewo = calloc (1, sizeof(BinaryTree));

    typedef struct BinaryTree {
        int value;
        struct BinaryTree* right;
        struct BinaryTree* left;
    } BinaryTree;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                2. Funkcje dodatkowe.                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Zamiana dwóch liczb całkowitych.

    void swapInts (int* a, int* b) {
        int temp;
        temp = *a;
        *a = *b;
        *b = temp;
    }

// Zamiana dwóch zmiennych znakowych.

    void swapChars (int* a, int* b) {
        char temp;
        temp = *a;
        *a = *b;
        *b = temp;
    }

// Zamiana dwóch liczb zmiennoprzecinkowych.

    void swapFloats (float* a, float* b) {
        float temp;
        temp = *a;
        *a = *b;
        *b = temp;
    }

// Przeliczenie liczby cyfr w danej liczbie:
//
// int number - dana liczba.
//
// Funkcja wlicza do wyniku znak, jeżeli liczba jest ujemna.

    int intLen (int number) {
        int result = 1;
        if (number < 0) {
            number *= -1;
            result++;
        }

        if (number >= 1000000000)
            return 10;
        if (number == 0)
            return 1;

        int i;
        for (i = 10; i <= 1000000000; i *= 10) {
            if (number >= (i / 10) && number < i)
                return result;
            result++;
        }
    }

// Funkcja losuje liczbę całkowitą z podanego przedziału:
//
// int from - początek przedziału,
// int to - koniec przedziału.
//
// Przed użyciem tej funkcji należy ustawić ziarno funkcją srand.
// Funkcja zwraca losową liczbę zmiennoprzecinkową.

    int getRandomInt (int from, int to) {
        if (from > to)
            swapInts (&from, &to);

        int result = (rand() % ((to + 1) - from)) + from;

        if (result < from || result > to)
            errorHandler("getRandomInt", 1, VALUE_EXCEEDS_RANGE);

        return result;
    }

// Funkcja losuje liczbę zmiennoprzecinkową z podanego przedziału:
//
// float from - początek przedziału,
// float to - koniec przedziału.
//
// Przed użyciem tej funkcji należy ustawić ziarno funkcją srand.
// Funkcja zwraca losową liczbę zmiennoprzecinkową.

    float getRandomFloat (float from, float to) {
        float temp;
        if (from > to)
            swapFloats(&from, &to);

        temp = from;
        to = to - from;
        from = 0;

        float result = ((float)rand()/(float)(RAND_MAX/to)) + temp;
        from += temp;
        to += temp;

        if (result < from || result > to)
            errorHandler("getRandomFloat", 1, VALUE_EXCEEDS_RANGE);

        return result;
    }

// Funkcja mieszająca tablicę całkowicie losowo:
//
// int array[] - tablica, która zostanie przemieszana,
// int length - liczba elementów tej tablicy,
// unsigned int numberOfShuffles - liczba losowych przemieszań.
//
// Przed użyciem tej funkcji należy ustawić ziarno funkcją srand.

    void shuffleNaive (int array[], int length, unsigned int numberOfShuffles) {
        if (length <= 0) {
            errorHandler("shuffleNaive", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int i;
        for (i = 0; i < numberOfShuffles; i++)
            swapInts(&array[getRandomInt(0, length - 1)], &array[getRandomInt(0, length - 1)]);
    }

// Funkcja mieszająca tablicę algorytmem Fishera-Yatesa:
//
// int array[] - tablica, która zostanie przemieszana,
// int length - liczba elementów tej tablicy.
//
// Przed użyciem tej funkcji należy ustawić ziarno funkcją srand.

    void shuffleFisherYates (int array[], int length) {
        if (length <= 0) {
            errorHandler("shuffleFisherYates", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        length--;
        while (length) {
            swapInts(&array[getRandomInt(0, length - 1)], &array[length]);
            length--;
        }
    }

// Wypelnienie tabliy losowymi elementami:
//
// int array[] - tablica, która zostanie wypełniona,
// int length - liczba elementów tej tablicy,
// int from, to - tablica zostanie wypełniona liczbami z zakresu <from, to>.

    void randFill (int array[], int length, int from, int to) {
        if (length <= 0) {
            errorHandler("randFill", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        if (from > to)
            swapInts (&from, &to);
        srand(time(NULL));
        int i;
            for (i = 0; i < length; i++)
                array[i] = (rand() % ((to + 1) - from)) + from;
    }

// Wypelnienie tabliy dwuwymiarowej losowymi elementami:
//
// int* matrix[] - macierz, która zostanie wypełniona,
// int lines - liczba wierszy,
// int columns - liczba kolumn,
// int from, to - macierz zostanie wypełniona liczbami z zakresu <from, to>.

    void randFillMatrix (int* matrix[], int lines, int columns, int from, int to) {
        if (columns <= 0 || lines <= 0) {
            errorHandler("randFillMatrix", 0, INCORRECT_MATRIX_SIZE);
            return;
        }

        if (from > to)
            swapInts (&from, &to);
        srand(time(NULL));
            int l, c;
                for (l = 0; l < lines; l++)
                    for (c = 0; c < columns; c++)
                        matrix[l][c] = (rand() % ((to + 1) - from)) + from;
    }

// Znalezienie losowych składników sumy danej liczby:
//
// int componentsAmount - liczba składników,
// int sum - dana liczba,
// int minValue - minimalna wartość składnika,
// int maxValue - maksymalna wartość składnika.
//
// Funkcja zwraca tablicę liczb całkowitych, której liczba elementów jest
// równa wartości componentsAmount.
// Funkcja zwraca NULL jeżeli liczba składników jest zbyt duża.

    int* getRandomSumComponents (int componentsAmount, int sum, int minValue, int maxValue) {
        if ((componentsAmount <= 0) || (componentsAmount * minValue > sum)) {
            errorHandler("getRandomSumComponents", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        int* result = malloc (sizeof(int) * componentsAmount);
        int i;
        int currentSum = 0;
        int difference, addition, randomIndex;

        if (minValue >= (sum /componentsAmount))
            for (i = 0; i < componentsAmount; i++)
                result[i] = (sum / componentsAmount);

        else {
            for (i = 0; i < componentsAmount; i++)
                result[i] = getRandomInt(minValue, maxValue);

            for (i = 0; i < componentsAmount; i++)
                currentSum += result[i];

            if (currentSum / sum != 0)
                for (i = 0; i < componentsAmount; i++)
                    result[i] /= (currentSum / sum);
        }

        currentSum = 0;
        for (i = 0; i < componentsAmount; i++)
            currentSum += result[i];

        difference = currentSum - sum;

        while (difference != 0) {
            addition = getRandomInt(0, difference);
            randomIndex = getRandomInt(0, (componentsAmount - 1));
            if (result[randomIndex] - addition >= minValue && result[randomIndex] - addition <= maxValue) {
                result[randomIndex] -= addition;
                difference -= addition;
            }
        }

        /* Sprawdzenie poprawności działania funkcji.
            currentSum = 0;
            for (i = 0; i < componentsAmount; i++)
                currentSum += result[i];
            if (currentSum != sum) {
                fprintf(stderr, "Error in function getRandomSumComponents: sum differs from that given in function argument!\n");
                fprintf(stderr, "Given %d, have %d.", sum, currentSum);
            }
        //*/

        return result;
    }

// Zadeklarowanie pamięci dla kolumn tablicy dwuwymiarowej przy użyciu funkcji malloc:
//
// int* matrix[] - macierz, dla której zostaną utworzone kolumny,
// int lines - liczba wierszy,
// int columnsToCreate - liczba kolumn, które zostaną utworzone.

    void expandMatrix (int* matrix[], int lines, int columnsToCreate) {
        if (columnsToCreate <= 0 || lines <= 0) {
            errorHandler("expandMatrix", 0, INCORRECT_MATRIX_SIZE);
            return;
        }

        int l;
        for (l = 0; l < lines; l++)
            matrix[l] = (int*) malloc (sizeof(int) * columnsToCreate);
    }

// Przepisanie tablicy dwuwymiarowej do tablicy jednowymiarowej:
//
// int* matrix[] - macierz, która zostanie przepisana,
// int lines - liczba wierszy,
// int columns - liczba kolumn,
// int result[] - utworzona tablica jednowymiarowa,
// char leftToRight - jeżeli 0 - przepisywanie od prawej do lewej, w przeciwnym wypadku odwrotnie,
// char upToDown - jeżeli 0 - przepisywanie od dołu do góry, w przeciwnym wypadku odwrotnie.

    void projectMatrix (int* matrix[], int lines, int columns, int result[], char leftToRight, char upToDown) {
        if (columns <= 0 || lines <= 0) {
            errorHandler("projectMatrix", 0, INCORRECT_MATRIX_SIZE);
            return;
        }

        int i = 0, l, c;
        int newIndexA, newIndexB;

        for (l = 0; l < lines; l++)
            for (c = 0; c < columns; c++) {
                if (upToDown) newIndexA = l;
                else newIndexA = (lines - l - 1);
                if (leftToRight) newIndexB = c;
                else newIndexB = (columns - c - 1);

                result[columns * newIndexA + newIndexB] = matrix[l][c];
            }
    }

// Przepisanie tablicy jednowymiarowej do tablicy dwuwymiarowej:
//
// int array[] - tablica, która zostanie przepisana,
// int lines - liczba wierszy,
// int columns - liczba kolumn,
// int* result[] - utworzona macierz,
// char leftToRight - jeżeli 0 - przepisywanie od prawej do lewej, w przeciwnym wypadku odwrotnie,
// char upToDown - jeżeli 0 - przepisywanie od dołu do góry, w przeciwnym wypadku odwrotnie.

    void reprojectMatrix (int array[], int lines, int columns, int* result[], char leftToRight, char upToDown) {
        if (columns <= 0 || lines <= 0) {
            errorHandler("reprojectMatrix", 0, INCORRECT_MATRIX_SIZE);
            return;
        }

        int i = 0, l, c;
        int newIndexA, newIndexB;

        for (l = 0; l < lines; l++)
            for (c = 0; c < columns; c++) {
                if (upToDown) newIndexA = l;
                else newIndexA = (lines - l - 1);
                if (leftToRight) newIndexB = c;
                else newIndexB = (columns - c - 1);

                result[l][c] = array[columns * newIndexA + newIndexB];
            }
    }

// Specyficzne (po skosie) przepisanie tablicy dwuwymiarowej do tablicy jednowymiarowej:
//
// int* matrix[] - macierz, która zostanie przepisana,
// int lines - liczba wierszy,
// int columns - liczba kolumn,
// int result[] - utworzona tablica jednowymiarowa.
//
// Elementy są przepisywane jak na poniższym przykładzie:
// 0  1  3  6
// 2  4  7  10
// 5  8  11 13
// 9  12 14 15

    void projectMatrixFromCorner (int* matrix[], int lines, int columns, int result[]) {
        if (columns <= 0 || lines <= 0) {
            errorHandler("projectMatrixFromCorner", 0, INCORRECT_MATRIX_SIZE);
            return;
        }

        int x, y;
        int l, c;
        int index = 0;

        for (y = 0; y < columns; y++) {
            l = 0;
            for (c = y; c >= 0 && l < lines; c--) {
                result[index] = matrix[l][c];
                l++;
                index++;
            }
        }

        for (x = 1; x < lines; x++) {
            c = columns - 1;
            for (l = x; l < lines && c >= 0; l++) {
                result[index] = matrix[l][c];
                c--;
                index++;
            }
        }
    }

// Specyficzne (po skosie) przepisanie tablicy jednowymiarowej do tablicy dwuwymiarowej:
//
// int array[] - tablica, która zostanie przepisana,
// int lines - liczba wierszy,
// int columns - liczba kolumn,
// int* result[] - utworzona macierz.
//
// Elementy są przepisywane jak na poniższym przykładzie:
// [ 0, 1, 3, 6, 2, 4, 7, 10, 5, 8, 11, 13, 9, 12, 14, 15 ]

    void reprojectMatrixToCorner (int array[], int lines, int columns, int* result[]) {
        if (columns <= 0 || lines <= 0) {
            errorHandler("reprojectMatrixToCorner", 0, INCORRECT_MATRIX_SIZE);
            return;
        }

        int x, y;
        int l, c;
        int index = 0;

        for (y = 0; y < columns; y++) {
            l = 0;
            for (c = y; c >= 0 && l < lines; c--) {
                result[l][c] = array[index];
                l++;
                index++;
            }
        }

        for (x = 1; x < lines; x++) {
            c = columns - 1;
            for (l = x; l < lines && c >= 0; l++) {
                result[l][c] = array[index];
                c--;
                index++;
            }
        }
    }

// Wyświetlenie tablicy wg schematu podanego na zajęciach:
//
// int array[] - tablica, która zostanie wyświetlona,
// int length - liczba elementów tej tablicy.

    void showArray (int array[], int length) {
        if (length < 0) {
            errorHandler("showArray", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        if (length == 0)
            printf("[");

        int i, chars = 1;
            printf("[");
            for (i = 0; i < length; i++) {
                if (chars + intLen(array[i]) + 2 > 80) {
                    printf("\n ");
                    chars = 1;
                }
                printf(" %d,", array[i]);
                chars += intLen(array[i]) + 2;
            }
            printf("\b ]");
    }

// Wyświetlenie tablicy dwuwymiarowej wg schematu podanego na zajęciach:
//
// int* matrix[] - macierz, która zostanie wyświetlona,
// int lines - liczba wierszy,
// int columns - liczba kolumn.
//
// Uwaga! Najlepiej wyświetlane są macierze, których elementy mają równą
// liczbę cyfr.

    void showMatrix (int* matrix[], int lines, int columns) {
        if (columns < 0 || lines < 0) {
            errorHandler("showMatrix", 0, INCORRECT_MATRIX_SIZE);
            return;
        }

        if (columns == 0 || lines == 0) {
            printf("[ ]");
            return;
        }

        int l, c, chars;
            for (l = 0; l < lines; l++) {
                chars = 1;
                printf("[");
                for (c = 0; c < columns; c++) {
                    if (chars + intLen(matrix[l][c]) + 2 > 78) {
                        printf(" -\n ");
                        chars = 1;
                    }
                    printf(" %d,", matrix[l][c]);
                    chars += intLen(matrix[l][c]) + 2;
                }
                if (l != lines - 1) printf("\b ]\n");
            }
            printf("\b ]");
    }

// Wyświetlenie wyników wyszukiwania tekstu:
//
// char string[] - tekst, który był przeszukiwany,
// int* result - tablica wyników zwrócona przez funkcję wyszukiwania.

    void showStringSearchResults (char string[], int* result) {
        int i;
        puts(string);
        if (result[0] == 0)
            printf("Brak trafień!");
        for (i = 1; i <= result[0]; i++) {
            int j;
            if (i == 1) for (j = 0; j < result[i]; j++)
                printf(" ");
            else for (j = result[i - 1] + 1; j < result[i]; j++) {
                printf(" ");
            }
            printf("^");
        }
    }

// Wyświetlenie dokładnych wyników wyszukiwania tekstu:
//
// char string[] - tekst, który był przeszukiwany,
// int* result - tablica wyników zwrócona przez funkcję wyszukiwania,
// int patternLength - liczba znaków we wzorcu.

    void showStringSearchWholeResults (char string[], int* result, int patternLength) {
        if (patternLength <= 0) {
            errorHandler("showStringSearchWholeResults", 0, NOT_SPECIFIED);
            return;
        }

        int i;
        puts(string);
        if (result[0] == 0)
            printf("Brak trafień!");
        for (i = 1; i <= result[0]; i++) {
            int j;
            if (i == 1) for (j = 0; j < result[i]; j++)
                printf(" ");
            else {
                for (j = result[i - 1] + 1; j < result[i]; j++)
                    printf(" ");
                for (j = 0; j < patternLength - 1; j++)
                    printf("\b");
            }
            for (j = 0; j < patternLength; j++) printf("^");
        }
    }

// Wyświetlenie znaków zakodowanych w tablicy wg schematu podanego na zajęciach:
//
// int array[] - tablica, która zostanie wyświetlona,
// int length - liczba elementów tej tablicy.

    void showArrayAsChars (int array[], int length) {
        if (length < 0) {
            errorHandler("showArrayAsChars", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        if (length == 0) printf("[");
        int i, chars = 1;
            printf("[ ");
            for (i = 0; i < length; i++) {
                if (chars + intLen(array[i]) + 2 > 80) {
                    printf("\n ");
                    chars = 1;
                }
                printf("%c", array[i]);
                chars += intLen(array[i]) + 2;
            }
            printf(" ]");
    }

// Wyświetlenie znaków zakodowanych w tablicy dwuwymiarowej wg schematu podanego na zajęciach:
//
// int* matrix[] - macierz, która zostanie wyświetlona,
// int lines - liczba wierszy,
// int columns - liczba kolumn.
//
// Uwaga! Najlepiej wyświetlane są macierze, których elementy mają równą
// liczbę cyfr.

    void showMatrixAsChars (int* matrix[], int lines, int columns) {
        if (columns < 0 || lines < 0) {
            errorHandler("showMatrixAsChars", 0, INCORRECT_MATRIX_SIZE);
            return;
        }

        if (columns == 0 || lines == 0) {
            printf("[ ]");
            return;
        }

        int l, c, chars;
            for (l = 0; l < lines; l++) {
                chars = 1;
                printf("[");
                for (c = 0; c < columns; c++) {
                    if (chars + intLen(matrix[l][c]) + 2 > 78) {
                        printf(" -\n ");
                        chars = 1;
                    }
                    printf(" %c,", matrix[l][c]);
                    chars += intLen(matrix[l][c]) + 2;
                }
                if (l != lines - 1) printf("\b ]\n");
            }
            printf("\b ]");
    }

// Odwrócenie tablicy (ostatni element jest pierwszym itd.):
//
// int array[] - tablica, która zostanie odwrócona,
// int length - liczba elementów danej tablicy.

    void reverseArray (int array[], int length) {
        if (length <= 0) {
            errorHandler("reverseArray", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int limA, limB;
        if (length == 1) return;
        if (length % 2 == 0) {
            limA = length / 2 - 1;
            limB = length / 2;
        }
        if (length % 2 != 0) {
            limA = length / 2;
            limB = length / 2 + 2;
        }

        int i = 0, j = length - 1;

        while (i <= limA && j >= limB) {
            swapInts(&array[i], &array[j]);
            i++;
            j--;
        }
    }

// Porównanie dwóch tablic:
//
// int arrayA[], arrayB[] - tablice liczb całkowitych, które będą porównywane,
// int length - liczba elementów danych tablic.
//
// Funkcja zwraca 1, jeżeli tablice są identyczne, w przeciwnym wypadku - 0.

    int cmpArrays (int arrayA[], int arrayB[], int length) {
        if (length <= 0) {
            errorHandler("cmpArrays", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        int i;
        int result = 1;

        for (i = 0; i < length && result != 0; i++) {
            if (arrayA[i] != arrayB[i]) result = 0;
        }

        return result;
    }

// Porównanie dwóch tablic i zapisanie indeksów elementów, które się od siebie różnią:
//
// int arrayA[], arrayB[] - tablice liczb całkowitych, które będą porównywane,
// int length - liczba elementów danych tablic.
//
// Funkcja zwraca tablicę liczb całkowitych. Jej pierwszy element
// to liczba różnic, każdy kolejny - indeks znalezionej różnicy.

    int* cmpArraysSaveDif (int arrayA[], int arrayB[], int length) {
        if (length <= 0) {
            errorHandler("cmpArraysSaveDif", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        int* result = (int*) malloc (sizeof(int));
        result[0] = 0;
        int i;

        for (i = 0; i < length; i++) {
            if (arrayA[i] != arrayB[i]) {
                result[0]++;
                result = (int*) realloc (result, sizeof(int) * result[0] + sizeof(int));
                result[result[0]] = i;
            }
        }

        return result;
    }

// Przeszukiwanie całej tablicy w celu odnalezienia jej maksymalnego / minimalnego elementu:
//
// int array[] - tablica liczb całkowitych, która będzie przeszukiwana,
// int length - liczba elementów danej tablicy,
// char getMax - szukanie maksymalnego elementu jeżeli wartość tej zmiennej jest różna od zera.

    int getExtreme (int array[], int length, char getMax) {
        if (length <= 0) {
            errorHandler("getExtreme", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        int i;
        int extremeValue = array[0];

            for (i = 1; i < length; i++)
                if ((array[i] > extremeValue && getMax) || (array[i] < extremeValue && !getMax))
                    extremeValue = array[i];

        return extremeValue;
    }

// Przesunięcie elementów tablicy:
//
// int array[] - dana tablica,
// int length - liczba elementów tablicy,
// int value - wartość przesunięcia (jeżeli ujemna - przesunięcie w lewą stronę).
//
// Uwaga! Funkcja nie jest zoptymalizowana; dla wartości przesunięcia
// bliskiej długości tablicy i dla niewielkich wartości ujemnych rozmiar
// stosu jest bliski rozmiarowi tablicy!

    void shiftArray (int array[], int length, int value) {
        if (length <= 0) {
            errorHandler("shiftArray", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        char direction = 'r';
        if (value < 0) {
            value *= -1;
            direction = 'l';
        }

        if (value >= length) value = value % length;

        if (direction == 'l') value = length - value;

        if (value == 0) return;

        int stack[value];
        int i;
        int j = value - 1;

        for (i = length - 1; i >= 0; i--) {
            if (j >= 0) {
                stack[j] = array[i];
                j--;
            }
            array[i] = array[i - value];
        }

        for (i = 0; i < value; i++) array[i] = stack[i];
    }

// Funkcja liczy i zwraca czas sortowania danym algorytmem:
//
// void(*sortingMethod)(int, int, char) - dana funkcja sortująca,
// int array[] - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.
//
// Funkcja nie liczy czasu działania funkcji sort01 oraz sort012.

    int getSortTime (void(*sortingMethod)(int, int, char), int array[], int length, char isAscending) {
        int executionTime = time(NULL);
        sortingMethod(array, length, isAscending);
        return (time(NULL) - executionTime);
    }

// Dobranie końcówek do słów "godzin", "minut" i "sekund" w zależności od ich liczby:
//
// int hours - liczba godzin,
// int minutes - liczba minut,
// int seconds - liczba sekund.
//
// Funkcja tablicę trzech znaków: końcówki "a" lub "y", jeżeli końcówka nie istnieje - nullbajt.

    char* getInfixes (int hours, int minutes, int seconds) {
        char *result = (char*) malloc (sizeof(char) * 3);

        // Odmiana godzin.
        if (hours == 0) result[0] = '\0';
        if (hours == 1) result[0] = 'a';
        if (hours > 1 && hours < 5) result[0] = 'y';
        if (hours > 4 && hours < 22) result[0] = '\0';
        if (hours > 21 && hours < 25) result[0] = 'y';

        // Odmiana minut.
        if (minutes == 0) result[1] = '\0';
        if (minutes == 1) result[1] = 'a';
        if (minutes > 1 && minutes < 5) result[1] = 'y';
        if (minutes > 4 && minutes < 20) result[1] = '\0';
        if (minutes > 19 && ((minutes % 10 == 2) || (minutes % 10 == 3) || (minutes % 10 == 4))) result[1] = 'y';
        else if ((minutes > 19) && !((minutes % 10 == 2) || (minutes % 10 == 3) || (minutes % 10 == 4))) result[1] = '\0';

        // Odmiana sekund (taka sama jak odmiana minut).
        if (seconds == 0) result[2] = '\0';
        if (seconds == 1) result[2] = 'a';
        if (seconds > 1 && seconds < 5) result[2] = 'y';
        if (seconds > 4 && seconds < 20) result[2] = '\0';
        if (seconds > 19 && ((seconds % 10 == 2) || (seconds % 10 == 3) || (seconds % 10 == 4))) result[2] = 'y';
        else if ((seconds > 19) && !((seconds % 10 == 2) || (seconds % 10 == 3) || (seconds % 10 == 4))) result[2] = '\0';

        return result;
    }

// Wyświetlenie czasu w formacie "X godzin, Y minut i Z sekund" na podstawie danej liczby
// sekund, dodając do słów "godzin", "minut" oraz "sekund" odpowiednie końcówki:
//
// int seconds - dana liczba sekund.

    void printTime (int seconds) {
        int hours = seconds / 3600;
        int minutes = (seconds - (hours * 3600)) / 60;
        seconds = seconds - (hours * 3600) - (minutes * 60);

        char* infix = (char*) malloc (sizeof(char) * 3);

        infix = getInfixes (hours, minutes, seconds);

        if (hours + minutes + seconds == 0) printf("mniej niż sekunda");
        if (hours > 0) printf("%d godzin%c", hours, infix[0]);
        if (hours > 0 && minutes > 0) printf(", ");
        if (minutes > 0) printf("%d minut%c", minutes, infix[1]);
        if ((hours > 0 || minutes > 0) && seconds > 0) printf(" i ");
        if (seconds > 0) printf("%d sekund%c", seconds, infix[2]);
    }

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                           3. Algorytmy sortujące tablicę.                                           *
 *                                                                                                                     *
 *                              Zwracana wartość to czas wykonywania algorytmu w sekundach.                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Sortowanie metodą flagi polskiej:
//
// int array[] - dana tablica, w której powinny znajdować się jedynie zera lub jedynki,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sort01 (char array[], int length, char isAscending) {
        if (length <= 0) {
            errorHandler("sort01", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int left = 0;
        int right = length - 1;
        int temp;

        do {
            while ((array[left] == 0 && left < right && isAscending) || (array[left] != 0 && left < right && !isAscending)) left++;
            while ((array[right] == 1 && left < right && isAscending) || (array[right] != 1 && left < right && !isAscending)) right--;
            if (left < right) swapInts(&array[left], &array[right]);
        } while (left < right);
    }

// Sortowanie metodą flagi francuskiej:
//
// int array[] - dana tablica, w której powinny znajdować się jedynie zera, jedynki i dwójki,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sort012 (char array[], int length, char isAscending) {
        if (length <= 0) {
            errorHandler("sort012", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int left = -1;
        int middle = 0;
        int right = length;

        while (middle < right) {
            if ((array[middle] == 0 && isAscending) || (array[middle] == 2 && !isAscending)) {
                left++;
                swapInts(&array[left], &array[middle]);
                middle++;
            }

            else if (array[middle] == 1) middle++;

            else {
                right--;
                swapInts(&array[middle], &array[right]);
            }
        }
    }

// Sortowanie bąbelkowe:
//
// int array[] - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortBubble (int array[], int length, char isAscending) {
        if (length <= 0) {
            errorHandler("sortBubble", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int N = length - 1;
        int temp;
        int i;

        for (; N > 0; N--) {
            for (i = 0; i < N; i++) {
                if (((isAscending) && array[i] > array[i + 1]) || ((!isAscending) && array[i] < array[i + 1]))
                    swapInts (&array[i], &array[i + 1]);
            }
        }
    }

// Sortowanie gnoma:
//
// int array[] - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortGnome (int array[], int length, char isAscending) {
        if (length <= 0) {
            errorHandler("sortGnome", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int i = 1;

        while (i < length) {
            if ((array[i] >= array[i - 1] && isAscending) || (array[i] <= array[i - 1] && !isAscending)) ++i;

            else {
                swapInts(&array[i], &array[i - 1]);
                --i;
                if (i == 0) i++;
            }
        }
    }

// Sortowanie przez wstawianie:
//
// int array[] - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortInsertion (int array[], int length, char isAscending) {
        if (length <= 0) {
            errorHandler("sortInsertion", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int key;
        int i, j;

        for (i = 1; i < length; ++i) {
            key = array[i];

            for (j = i - 1; j >= 0 && (((isAscending) && array[j] > key) || (!(isAscending) && array[j] < key)); --j) {
                array[j + 1] = array[j];
            }

            array[j + 1] = key;
        }
    }

// Sortowanie przez wybranie minimalnej wartości:
//
// int array[] - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortSelection (int array[], int length, char isAscending) {
        if (length <= 0) {
            errorHandler("sortSelection", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int i, j;
        int iMin;

        for (j = 0; j < length - 1; j++) {
            iMin = j;
            for (i = j + 1; i < length; i++) {
                if (((isAscending) &&array[i] < array[iMin]) || (!(isAscending) &&array[i] > array[iMin])) {
                    iMin = i;
                }
            }
            if (iMin != j) {
                swapInts (&array[j], &array[iMin]);
            }
        }
    }

// Sortowanie grzebieniowe:
//
// int array[] - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortComb (int array[], int length, char isAscending) {
        if (length <= 0) {
            errorHandler("sortComb", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int gap = length;
        char swapped;
        int i;

        do {
            swapped = 0;

            gap /= 1.3;
            if (gap == 9 || gap == 10) gap = 11;
            if (gap < 1) gap = 1;

            for (i = 0; i < length - gap; ++i) {
                if ((array[i] > array[i + gap] && isAscending) || (array[i] < array[i + gap] && !isAscending)) {
                    swapped = 1;
                    swapInts(&array[i], &array[i + gap]);
                }
            }
        } while (gap > 1 || swapped);
    }

// Sortowanie szybkie:
//
// int array[] - dana tablica dowolnych liczb całkowitych,
// int length - liczba elementów danej tablicy,
// char isAscending - sortowanie rosnące jeżeli wartość tej zmiennej jest różna od zera.

    void sortQuick (int array[], int length, char isAscending) {
        if (length <= 0) {
            errorHandler("sortQuick", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        sortQuickPartially(array, 0, length, isAscending);
    }

// Właściwe sortowanie szybkie. Wygodniejsza w użyciu jest funkcja powyżej.

    void sortQuickPartially (int array[], int begin, int end, char isAscending) {
        int pivotIndex = begin + (end - begin) / 2;
        int pivot;

        if (begin < end) {
            int left = begin + 1;
            int right = end;

            swapInts(&array[begin], &array[pivotIndex]);
            pivot = array[begin];

            while (left < right) {
                if ((array[left] <= pivot && isAscending) || (array[left] >= pivot && !isAscending)) left++;
                else {
                    if (isAscending) while (left < --right && array[right] >= pivot);
                    else while (left < --right && array[right] <= pivot);
                    swapInts(&array[left], &array[right]);
                }
            }

            left--;
            swapInts(&array[begin], &array[left]);

            sortQuickPartially(array, begin, left, isAscending);
            sortQuickPartially(array, right, end, isAscending);
        }
    }

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                        4. Algorytmy przeszukujące tablicę.                                          *
 *                                                                                                                     *
 *                                   Zwracana wartość to indeks szukanego elementu                                     *
 *                                   lub (-1), jeżeli element nie został znaleziony.                                   *
 *                                      Indeks pierwszego elementu tablicy to 0.                                       *
 *                                 Wartością zwracaną przez funkcje przeszukujące ciąg                                 *
 *                                 znaków jest jednowymiarowa tablica indeksów trafień,                                *
 *                                   której pierwszym elementem jest liczba trafień.                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Wyszukiwanie liniowe:
//
// int array[] - tablica liczb całkowitych, która będzie przeszukiwana,
// int length - liczba elementów danej tablicy,
// int searchedValue - szukany element tablicy.

    int searchLinear (int array[], int length, int searchedValue) {
        if (length <= 0) {
            errorHandler("searchLinear", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        int i;
            for (i = 0; i < length; i++)
                if (array[i] == searchedValue) return i;

        return (-1);
    }

// Wyszukiwanie liniowe i zapisanie indeksów znalezionych elementów:
//
// int array[] - tablica liczb całkowitych, która będzie przeszukiwana,
// int length - liczba elementów danej tablicy,
// int searchedValue - szukany element tablicy.
//
// Funkcja zwraca tablicę liczb całkowitych. Jej pierwszy element
// to liczba trafień, każdy kolejny - indeks znalezionego elementu.

    int* searchLinearSaveRes (int array[], int length, int searchedValue) {
        if (length <= 0) {
            errorHandler("searchLinearSaveRes", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        int* result = (int*) malloc (sizeof(int));
        result[0] = 0;

        int i;
            for (i = 0; i < length; i++)
                if (array[i] == searchedValue) {
                    result[0]++;
                    result = (int*) realloc(result, sizeof(int) * result[0] + sizeof(int));
                    result[result[0]] = i;
                }

        return result;
    }

// Wyszukiwanie binarne (tylko na posortowanej tablicy):
//
// int sortedArray[] - posortowana tablica liczb całkowitych, która będzie przeszukiwana,
// int length - liczba elementów danej tablicy,
// int searchedValue - szukany element tablicy.

    int searchBinary (int sortedArray[], int length, int searchedValue) {
        if (length <= 0) {
            errorHandler("searchBinary", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        char isAscending = 1;
            if (sortedArray[0] > sortedArray[length - 1]) isAscending = 0;

        int left = 0;
        int right = length - 1;
        int middle;
        int middleElement;

            while (left <= right) {
                middle = (left + right) / 2;
                middleElement = sortedArray[middle];

                if (middleElement == searchedValue)
                    return middle;

                else if ((middleElement < searchedValue && isAscending) || (middleElement > searchedValue && !isAscending))
                    left = middle + 1;

                else
                    right = middle - 1;
            }
        return (-1);
    }

// Wyszukiwanie interpolacyjne (tylko na posortowanej tablicy):
//
// int sortedArray[] - posortowana tablica liczb całkowitych, która będzie przeszukiwana,
// int length - liczba elementów danej tablicy,
// int searchedValue - szukany element tablicy.

    int searchInterpolating (int sortedArray[], int length, int searchedValue) {
        if (length <= 0) {
            errorHandler("searchInterpolating", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        char isAscending = 1;
            if (sortedArray[0] > sortedArray[length - 1]) isAscending = 0;

        int left = 0;
        int right = length - 1;
        int middle;
        int middleElement;

            while (left <= right) {
                middle = left + (searchedValue - sortedArray[left]) * (right - left) / (sortedArray[right] - sortedArray[left]);
                middleElement = sortedArray[middle];

                if (middleElement == searchedValue)
                    return middle;

                else if ((middleElement < searchedValue && isAscending) || (middleElement > searchedValue && !isAscending))
                    left = middle + 1;

                else
                    right = middle - 1;
            }
        return (-1);
    }

// Naiwne wyszukiwanie wzorca w danym tekście:
//
// char string[] - tablica znaków, która będzie przeszukiwana,
// int stringLength - liczba elementów danej tablicy znaków,
// char pattern[] - tablica znaków, która jest wzorcem,
// int patternLength - liczba znaków we wzorcu.
//
// Funkcja zwraca tablicę liczb całkowitych. Jej pierwszy element
// to liczba trafień, każdy kolejny - indeks pierwszego znaku wzorca
// znalezionego w tekście.

    int* searchStringNaive (char string[], int stringLength, char pattern[], int patternLength) {
        if (stringLength <= 0 || patternLength <= 0) {
            errorHandler("searchStringNaive", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        int* result = (int*) malloc (sizeof(int));
        result[0] = 0;
        char isMatching;

        int i;
        for (i = 0; i < stringLength - patternLength + 2; i++) {
            isMatching = 0;
            if (string[i] == pattern[0]) {
                isMatching = 1;
                int j;
                for (j = i + 1; j < i + patternLength && isMatching; j++) {
                    if (string[j] != pattern[j - i])
                        isMatching = 0;
                }
            }
            if (isMatching) {
                result[0]++;
                result = (int*) realloc (result, sizeof(int) * result[0] + sizeof(int));
                result[result[0]] = i;
            }
        }

        return result;
    }

// Wyszukiwanie wzorca w danym tekście algorytmem Karpa-Rabina:
//
// char string[] - tablica znaków, która będzie przeszukiwana,
// int stringLength - liczba elementów danej tablicy znaków,
// char pattern[] - tablica znaków, która jest wzorcem,
// int patternLength - liczba znaków we wzorcu,
// int hashingPrime - liczba pierwsza służąca do mieszania, np. 101.
//
// Funkcja zwraca tablicę liczb całkowitych. Jej pierwszy element
// to liczba trafień, każdy kolejny - indeks pierwszego znaku wzorca
// znalezionego w tekście.

    int* searchStringKR (char string[], int stringLength, char pattern[], int patternLength, int hashingPrime) {
        if (stringLength <= 0 || patternLength <= 0) {
            errorHandler("searchStringKR", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        int* result = (int*) malloc (sizeof(int));
        result[0] = 0;
        int i, j;
        int hashP = 0;
        int hashS = 0;
        int h = 1;

        for (i = 0; i < patternLength-1; i++)
            h = (h * alphabetLength) % hashingPrime;

        for (i = 0; i < patternLength; i++) {
            hashP = (alphabetLength * hashP + pattern[i]) % hashingPrime;
            hashS = (alphabetLength * hashS + string[i]) % hashingPrime;
        }

        for (i = 0; i <= stringLength - patternLength; i++) {
            if (hashP == hashS) {
                for (j = 0; j < patternLength; j++) {
                    if (string[i + j] != pattern[j])
                        break;
                }
                if (j == patternLength) {
                    result[0]++;
                    result = (int*) realloc (result, sizeof(int) * result[0] + sizeof(int));
                    result[result[0]] = i;
                }
            }

            if (i < stringLength - patternLength) {
                hashS = (alphabetLength * (hashS - string[i] * h) + string[i + patternLength]) % hashingPrime;

                if (hashS < 0)
                  hashS = (hashS + hashingPrime);
            }
        }

        return result;
    }

// Wyszukiwanie wzorca w danym tekście algorytmem Knutha-Morrisa-Pratta:
//
// char string[] - tablica znaków, która będzie przeszukiwana,
// int stringLength - liczba elementów danej tablicy znaków,
// char pattern[] - tablica znaków, która jest wzorcem,
// int patternLength - liczba znaków we wzorcu.
//
// Funkcja zwraca tablicę liczb całkowitych. Jej pierwszy element
// to liczba trafień, każdy kolejny - indeks pierwszego znaku wzorca
// znalezionego w tekście. Funkcja zwraca tablicę jednoelementową,
// której pierwszy element jest równy (-1) w przypadku, gdy wyznaczenie
// prefiksu się nie powiedzie.

    int* searchStringKMP (char string[], int stringLength, char pattern[], int patternLength) {
        if (stringLength <= 0 || patternLength <= 0) {
            errorHandler("searchStringKMP", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        int i;
        int* result = (int*) malloc (sizeof(int));
        result[0] = 0;

        int k = -1;
        i = 1;
        int *prefix = malloc(sizeof(int)*patternLength);

        prefix[0] = k;
        for (i = 1; i < patternLength; i++) {
            while (k > -1 && pattern[k + 1] != pattern[i])
                k = prefix[k];
            if (pattern[i] == pattern[k + 1])
                k++;
            prefix[i] = k;
        }

        k = -1;
        if (!prefix) {
            result[0] = -1;
            return result;
        }
        for (i = 0; i < stringLength; i++) {
            while (k > -1 && pattern[k + 1] != string[i])
                k = prefix[k];
            if (string[i] == pattern[k+1])
                k++;
            if (k == patternLength - 1) {
                result[0]++;
                result = (int*) realloc (result, sizeof(int) * result[0] + sizeof(int));
                result[result[0]] = i - k;
            }
        }

        return result;
    }

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                            5. Funkcje do struktur danych.                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Funkcja oblicza odległość pomiędzy dwoma punktami:
//
// Point A - punkt pierwszy,
// Point B - punkt drugi.
//
// Funkcja zwraca odległość - liczbę całkowitą.
/*
    float getDistanceBetweenPoints (Point A, Point B) {
        float tempX = B.x - A.x;
        float tempY = B.y - A.y;

        return (sqrt((tempX*tempX)+(tempY*tempY)));
    }
*/
// Funkcja wyznacza losowy punkt zawarty w danym okręgu:
//
// Point center - środek okręgu,
// int radius - promień okręgu.
//
// Funkcja zwraca strukturę Point.
/*
    Point getRandomPointInCircle (Point center, float radius) {
        Point result;

        float degree = getRandomFloat(0, 90);
        float distance = getRandomFloat(0, radius);
        int doReverse;

        doReverse = getRandomInt(0, 1);
        result.x = distance * cos(degree);
        if (doReverse)
            result.x = result.x * (-1);
        result.x += center.x;

        doReverse = getRandomInt(0, 1);
        result.y = distance * sin(degree);
        if (doReverse)
            result.y = result.y * (-1);
        result.y += center.y;

        return result;
    }
*/
// Położenie elementu na stosie:
//
// int value - wartość elementu,
// Stack** givenStack - dany stos.

    void push (int value, Stack** givenStack) {
        Stack* newStack = malloc (sizeof(Stack));

        newStack->value = value;
        newStack->node = *givenStack;

        *givenStack = newStack;
    }

// Pobranie elementu ze stosu:
//
// Stack** givenStack - dany stos,
// int* value - zmienna, do której zostanie zapisana wartość elementu.
//
// Funkcja zdejmuje (niszczy) element stosu!

    char pop (Stack** givenStack, int* value) {
        if (*givenStack == NULL)
            return 0;

        *value = (*givenStack)->value;
        Stack* oldStack = *givenStack;

        *givenStack = oldStack->node;
        free(oldStack);

        return 1;
    }

// Pobranie wartości danego elementu stosu bez niszczenia go:
//
// Stack* givenStack - dany stos,
// int depth - dany element (głębokość na jakiej szukamy go w stosie),
// int* result - zmienna, do której zostanie zapisana wartość danego elementu stosu.
//
// Gdy zmienna depth ma wartość mniejszą lub równą 0, szukanym elementem jest szczyt stosu.
// Funkcja zwraca i ustawia wynik na 0, jeżeli dany element nie istnieje, w przeciwnym
// wypadku funkcja zwraca 1.

    char safeGetNodeValue (Stack* givenStack, int depth, int* result) {
        if (depth < 0) depth = 0;
        int value;
        Stack* current = givenStack;

        int i;

        for (i = 0; current != NULL && i < depth; i++)
            current = current->node;

        if (current == NULL) {
            *result = 0;
            return 0;
        }

        else {
            *result = current->value;
            return 1;
        }
    }

// Przepisanie stosu do tablicy jednowymiarowej:
//
// Stack** givenStack - dany stos,
// int result[] - tablica, do której zostanie przepisany stos.
//
// Uwaga! Funkcja niszczy zawartość stosu.

    void projectStack (Stack** givenStack, int result[]) {
        char isFilled = 1;
        int value;

        int i;

        for (i = 0; isFilled; i++) {
            isFilled = pop(givenStack, &value);
            if (isFilled)
                result[i] = value;
        }
    }

// Przepisanie stosu do tablicy jednowymiarowej bez niszczenia stosu:
//
// Stack* givenStack - dany stos,
// int result[] - tablica, do której zostanie przepisany stos.

    void safeProjectStack (Stack* givenStack, int result[]) {
        int value;
        Stack* current = givenStack;

        int i;

        for (i = 0; current != NULL; i++) {
            result[i] = current->value;
            current = current->node;
        }
    }

// Przepisanie tablicy jednowymiarowej do stosu:
//
// int givenArray[] - dana tablica,
// int length - liczba elementów tej tablicy,
// Stack** result - stos, na który zostaną położone elementy tablicy.
//
// Funkcja przepisuje tablicę od ostatniego elementu do pierwszego.

    void reprojectStack (int givenArray[], int length, Stack** result) {
        if (length <= 0) {
            errorHandler("reprojectStack", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int value;

        int i;

        for (i = length - 1; i >= 0; i--)
            push(givenArray[i], result);
    }

// Znalezienie wysokości stosu:
//
// Stack* givenStack - dany stos.
//
// Funkcja lepsza od poniższej, nie przepisuje dwukrotnie stosu.

    int getStackHeight (Stack* givenStack) {
        int stackHeight;

        Stack* current = givenStack;

        for (stackHeight = 0; current != NULL; stackHeight++) {
            current = current->node;
        }

        return stackHeight;
    }

// Znalezienie wysokości stosu:
//
// Stack** givenStack - dany stos.
//
// Funkcja zwraca liczbę elementów stosu, stos nie jest niszczony, ale
// jest dwukrotnie przepisywany.

    int getStackHeightWorse (Stack** givenStack) {
        Stack* backup = NULL;
        int stackHeight = 0;
        int value = 0;
        char isFilled = 1;

        while (isFilled) {
            isFilled = pop(givenStack, &value);
            if (isFilled) {
                push(value, &backup);
                stackHeight++;
            }
        }

        isFilled = 1;

        while (isFilled) {
            isFilled = pop(&backup, &value);
            if (isFilled)
                push(value, givenStack);
        }

        return stackHeight;
    }

// Odwrócenie stosu:
//
// Stack** givenStack - dany stos.

    void reverseStack (Stack** givenStack) {
        Stack* backup = *givenStack;
        int value = 0;
        char isFilled = 1;

        while (isFilled) {
            isFilled = pop(givenStack, &value);
            if (isFilled)
                push(value, &backup);
        }

        isFilled = 1;

        while (isFilled) {
            isFilled = pop(&backup, &value);
            if (isFilled)
                push(value, givenStack);
        }
    }

// Usunięcie stosu (zwolnienie pamięci przez niego zajmowanej):
//
// Stack** givenStack - dany stos.

    void deleteStack (Stack** givenStack) {
        int temp;
        while (pop(givenStack, &temp));
    }

// Dodanie komórki na tył kolejki:
//
// int value - wartość nowej komórki,
// Queue** givenQueue - dana kolejka.
//
// Funkcja zwraca wskaźnik na nowo dodany element kolejki.

    Cell* enQueue (int value, Queue** givenQueue) {
        Cell* newCell = calloc (1, sizeof(Cell));
        newCell->value = value;

        newCell->predecessor = NULL;

        if ((*givenQueue)->back != NULL) {
            (*givenQueue)->back->predecessor = newCell;
            newCell->successor = (*givenQueue)->back;
            (*givenQueue)->back = newCell;
            (*givenQueue)->length++;
        }

        else {
            (*givenQueue)->back = newCell;
            (*givenQueue)->front = newCell;
            (*givenQueue)->length = 1;
        }

        return newCell;
    }

// Pobranie komórki z przodu kolejki:
//
// Queue** givenQueue - dana kolejka,
// int* value - zmienna, do której zostanie zapisana wartość komórki.
//
// Funkcja usuwa pobraną komórkę!

    char deQueue (Queue** givenQueue, int* value) {
        if ((*givenQueue)->front == NULL) {
            *value = 0;
            return 0;
        }

        int result = (*givenQueue)->front->value;

        Cell* newFront = (*givenQueue)->front->predecessor;

        if ((*givenQueue)->front->predecessor != NULL)
            (*givenQueue)->front->predecessor->successor = NULL;

        *value = (*givenQueue)->front->value;

        free((*givenQueue)->front);

        (*givenQueue)->front = newFront;

        (*givenQueue)->length--;

        return 1;
    }

// Znalezienie liczby komórek w kolejce:
//
// Queue* givenQueue - dana kolejka.

    int getQueueLength (Queue* givenQueue) {
        if (givenQueue->back == NULL)
            return 0;
        else
            return givenQueue->length;
    }

// Przepisanie kolejki do tablicy jednowymiarowej bez niszczenia kolejki:
//
// Queue* givenQueue - dana kolejka,
// int result[] - tablica, do której zostanie przepisana kolejka.

    void projectQueue (Queue* givenQueue, int result[]) {
        if (givenQueue->front == NULL)
            return 0;

        Cell* current = givenQueue->back;
        result[getQueueLength(givenQueue) - 1] = current->value;

        int i;
        for (i = getQueueLength(givenQueue) - 2; i >= 0; i--) {
            current = current->successor;
            result[i] = current->value;
        }
    }

// Przepisanie tablicy jednowymiarowej do kolejki:
//
// int givenArray[] - dana tablica,
// int length - liczba elementów tej tablicy,
// Queue* givenQueue - kolejka, do której zostaną przepisane elementy tablicy.

    void reprojectQueue (int givenArray[], int length, Queue** result) {
        if (length <= 0) {
            errorHandler("reprojectQueue", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int i;
        for (i = 0; i < length; i++)
            enQueue(givenArray[i], result);
    }

// Dodanie nowej komórki do kolejki w odniesieniu do innej komórki:
//
// int value - wartość nowej komórki,
// Queue* givenQueue - dana kolejka,
// Cell* referenceCell - komórka, w odniesieniu do której zostanie dodana nowa komórka,
// char direction - jeżeli zmienna ma wartość 'r', komórka zostanie dodana jako następnik,
//                  jeżeli zmienna ma wartość 'l', komórka zostanie dodana jako poprzednik.
//
// Funkcja nie dodaje komórek na początku i końcu kolejki.
// Do tego służą funkcje addCellToFront oraz enQueue.
// Funkcja zwraca 1, jeżeli operacja się powiedzie, w przeciwnym wypadku funkcja zwraca 0.

    char addCell (int value, Queue* givenQueue, Cell* referenceCell, char direction) {
        if (direction == 'r' && referenceCell->successor == NULL)
            return 0;
        if (direction == 'l' && referenceCell->predecessor == NULL)
            return 0;

        Cell* newCell = malloc (sizeof(Cell));

        newCell->value = value;

        if (direction == 'r') {
            newCell->predecessor = referenceCell;
            newCell->successor = referenceCell->successor;
            referenceCell->successor->predecessor = newCell;
            referenceCell->successor = newCell;
            givenQueue->length++;
            return 1;
        }

        else if (direction == 'l') {
            newCell->predecessor = referenceCell->predecessor;
            newCell->successor = referenceCell;
            referenceCell->predecessor->successor= newCell;
            referenceCell->predecessor = newCell;
            givenQueue->length++;
            return 1;
        }

        return 0;
    }

// Dodanie komórki z przodu kolejki:
//
// int value - wartość nowej komórki,
// Queue** givenQueue - dana kolejka.
//
// Funkcja zwraca wskaźnik na nową komórkę kolejki.

    Cell* addCellToFront (int value, Queue** givenQueue) {
        Cell* newCell = calloc (1, sizeof(Cell));
        newCell->value = value;

        newCell->successor = NULL;

        if ((*givenQueue)->front != NULL) {
            (*givenQueue)->front->successor = newCell;
            newCell->predecessor = (*givenQueue)->front;
            (*givenQueue)->front = newCell;
            (*givenQueue)->length++;
        }

        else {
            (*givenQueue)->back = newCell;
            (*givenQueue)->front = newCell;
            (*givenQueue)->length = 1;
        }

        return newCell;
    }

// Usunięcie kolejki (zwolnienie pamięci przez nią zajmowanej):
//
// Queue** givenQueue - dana kolejka.

    void deleteQueue (Queue** givenQueue) {
        int temp;
        while (deQueue(givenQueue, &temp));
    }

// Dodanie elementu do listy:
//
// int value - wartość dodawanego elementu,
// List* givenList - wskaźnik na pierwszy element danej listy,
// int predecessorIndex - indeks elementu poprzedzającego dodawany element.
//
// Funkcja zwraca wskaźnik na nowo dodany element listy.

    List* insertToList (int value, List* givenList, int predecessorIndex) {
        if (predecessorIndex < 0) {
            errorHandler("insertToList", 0, INCORRECT_INDEX);
            return;
        }

        int i;
        for (i = 0; i != predecessorIndex && givenList->successor != NULL; i++)
            givenList = givenList->successor;

        List* newIndex = malloc (sizeof(List));
        newIndex->value = value;
        newIndex->successor = givenList->successor;
        givenList->successor = newIndex;
        return newIndex;
    }

// Usunicie elementu z listy:
//
// List** givenList - wskaźnik na pierwszy element danej listy,
// int index - indeks elementu do usunięcia.

    char deleteFromList (List* givenList, int index) {
        if (index < 0) {
            errorHandler("deleteFromList", 0, INCORRECT_INDEX);
            return NULL;
        }

        int i;
        for (i = 0; i != index - 1; i++) {
            givenList = givenList->successor;
            if (givenList->successor == NULL && i < index - 1)
                return 0;
        }

        List* toDelete = givenList->successor;
        givenList->successor = givenList->successor->successor;
        free(toDelete);
        return 1;
    }

// Zmiana wartości danego elementu listy:
//
// int value - nowa wartość elementu,
// List* givenList - wskaźnik na pierwszy element danej listy,
// int index - indeks elementu, którego wartość zostanie zmieniona.
//
// Funkcja zwraca poprzednią wartość elementu.

    int setListValue (int value, List* givenList, int index) {
        if (index < 0) {
            errorHandler("setListValue", 0, INCORRECT_INDEX);
            return NULL;
        }

        int i;
        for (i = 0; i != index && givenList->successor != NULL; i++)
            givenList = givenList->successor;

        int oldValue = givenList->value;
        givenList->value = value;
        return oldValue;
    }

// Pobranie wartości danego elementu listy:
//
// List* givenList - wskaźnik na pierwszy element danej listy,
// int index - indeks elementu, którego wartość zostanie pobrana.
//
// Funkcja zwraca wartość danego elementu listy.

    int getListValue (List* givenList, int index) {
        if (index < 0) {
            errorHandler("getListValue", 0, INCORRECT_INDEX);
            return NULL;
        }

        int i;
        for (i = 0; i != index && givenList->successor != NULL; i++)
            givenList = givenList->successor;

        return givenList->value;
    }

// Zmiana liczby elementów listy:
//
// List* givenList - wskaźnik na pierwszy element danej listy,
// int newLength - liczba elementów listy.
//
// Funkcja nadaje nowym elementom wartość 0.
// Funkcja zwraca 0, jeżeli liczba elementów listy nie została zmieniona,
// 1 jeżeli liczba ta została zwiększona lub (-1), jeżeli liczba elementów
// listy została zmniejszona.

    char setListLength (List* givenList, int newLength) {
        if (newLength <= 0)
            return 0;

        int i = 0;
        if (givenList != NULL)
            while (givenList->successor != NULL) {
                givenList = givenList->successor;
                i++;
                if (i == newLength - 1) {
                    if (givenList->successor == NULL) {
                        return 0;
                    }
                    else {
                        List* current = givenList->successor;
                        givenList->successor = NULL;
                        while (current != NULL) {
                            List* toDelete = current;
                            current = current->successor;
                            free(toDelete);
                        }
                        return 1;
                    }
                }
            }

        else
            return 0;

        if (i >= newLength - 1)
            return 0;

        for (; i < newLength - 1; i++) {
            List* newIndex = calloc(1, sizeof(List));
            givenList->successor = newIndex;
            givenList = newIndex;
        }

        return 1;
    }

// Znalezienie liczby elementów listy:
//
// List* givenList - wskaźnik na pierwszy element danej listy,
//
// Funkcja zwraca liczbę elementów danej listy.

    int getListLength (List* givenList) {
        int result = 0;
        while (givenList != NULL)
            result++;

        return result;
    }

// Przepisanie listy do tablicy jednowymiarowej bez niszczenia listy:
//
// List* givenList - wskaźnik na pierwszy element danej listy,
// int result[] - tablica, do której zostanie przepisana lista.
//
// Funkcja zwraca 0, jeżeli lista nie zawiera elementów i nie może
// zostać przepisana, w przeciwnym wypadku funkcja zwraca 1.

    char projectList (List* givenList, int result[]) {
        if (givenList == NULL)
            return 0;

        int i;
        for (i = 0; givenList; i++) {
            result[i] = givenList->value;
            givenList = givenList->successor;
        }
        return 1;
    }

// Przepisanie tablicy jednowymiarowej do listy:
//
// int givenArray[] - tablica, która zostanie przepisana,
// int length - liczba elementów tablicy,
// List* result - wskaźnik na pierwszy element danej listy.
//
// Funkcja zwraca 0, jeżeli liczba elementów tablicy jest
// mniejsza lub równa zeru, w przeciwnym wypadku funkcja zwraca 1.
// Elementy są dopisywane do końca listy; nie jest to najbardziej
// optymalna metoda.

    char reprojectList (int givenArray[], int length, List* result) {
        if (length <= 0) {
            errorHandler("reprojectList", 0, INCORRECT_ARRAY_LENGTH);
            return NULL;
        }

        int lastIndex = getListLength(result) - 1;

        int i;
        for (i = 0; i < length; i++) {
            insertToList(givenArray[i], result, lastIndex);
            lastIndex++;
        }
        return 1;
    }

// Dodanie węzła do danego drzewa binarnego:
//
// int value - wartość nowego węzła,
// BinaryTree** givenTree - dane drzewo.
//
// Funkcja zwraca wskaźnik na nowo utworzony węzeł.

    BinaryTree* insert (int value, BinaryTree** givenTree) {
        BinaryTree* temp = NULL;

        if(*givenTree == NULL) {
            temp = malloc(sizeof(BinaryTree));
            temp->left = temp->right = NULL;
            temp->value = value;
            *givenTree = temp;
            return temp;
        }

        if(value < (*givenTree)->value)
            insert(value, &(*givenTree)->left);

        else if(value > (*givenTree)->value)
            insert(value, &(*givenTree)->right);
    }

// Znalezienie rozmiaru drzewa (liczby jego elementów):
//
// BinaryTree* givenTree - dane drzewo.
//
// Funkcja zwraca liczbę elementów danego drzewa.

    int getTreeSize (BinaryTree* givenTree) {
        if (givenTree == NULL)
            return;

        int result = 1;

        result += getTreeSize(givenTree->left);
        result += getTreeSize(givenTree->right);

        return result;
    }

// Wyszukiwanie węzła o danej wartości w drzewie binarnym:
//
// BinaryTree** givenTree - dane drzewo,
// int searchedValue - wyszukiwana wartość.
//
// Funkcja zwraca wskaźnik na znaleziony węzeł.

    BinaryTree* searchBinaryTree (BinaryTree** givenTree, int searchedValue) {
        if(*givenTree == NULL)
            return NULL;

        if(searchedValue < (*givenTree)->value)
            searchBinaryTree(&((*givenTree)->left), searchedValue);

        else if(searchedValue > (*givenTree)->value)
            searchBinaryTree(&((*givenTree)->right), searchedValue);

        else if(searchedValue == (*givenTree)->value)
            return *givenTree;
    }

// Przepisanie drzewa binarnego do tablicy jednowymiarowej:
//
// BinaryTree* givenTree - dane drzewo,
// int result[] - tablica jednowymiarowa, do której zostanie zapisany wynik (liczba elementów koniecznie
//                równa liczbie elementów drzewa),
// int* counter - wyzerowana (!) zmienna służaca za licznik elementów,
// char order - porządek, w jakim przepisywane jest drzewo, jeżeli zmienna jest równa
//              -1 - drzewo przepisywane od góry, najpierw przepisywane gałęzie po lewej (pre order),
//              0 - elementy przepisywane w kolejności rosnącej (in order),
//              1 - drzewo jest przepisywane od dołu (post order).
//
// Funkcja zwraca 0, jeżeli w drzewie nie ma żadnych elementów, w przeciwnym wypadku funkcja zwraca 1.

    char projectTree (BinaryTree* givenTree, int result[], int* counter, char order) {
        if (!givenTree)
            return 0;

        if (order == (-1)) {
                ++*counter;
                result[*counter - 1] = givenTree->value;
            projectTree(givenTree->left, result, counter, order);
            projectTree(givenTree->right, result, counter, order);
        }

        if (order == 0) {
            projectTree(givenTree->left, result, counter, order);
                ++*counter;
                result[*counter - 1] = givenTree->value;
            projectTree(givenTree->right, result, counter, order);
        }

        if (order == 1) {
            projectTree(givenTree->left, result, counter, order);
            projectTree(givenTree->right, result, counter, order);
                ++*counter;
                result[*counter - 1] = givenTree->value;
        }

        return 1;
    }

// Dopisanie tablicy jednowymiarowej do drzewa:
//
// int givenArray[] - dana tablica,
// int length - liczba elementów tablicy,
// BinaryTree** result[] - utworzone drzewo.

    void reprojectTree (int givenArray[], int length, BinaryTree** result) {
        if (length <= 0) {
            errorHandler("reprojectTree", 0, INCORRECT_ARRAY_LENGTH);
            return;
        }

        int i;
        for (i = 0; i < length; i++)
            insert(givenArray[i], result);
    }

// Usunięcie drzewa (zwolnienie pamięci przez nie zajmowanej):
//
// BinaryTree* givenTree - dane drzewo.

    void deleteTree (BinaryTree** givenTree) {
        if ((*givenTree)->left != NULL)
            deleteTree(&(*givenTree)->left);
        if ((*givenTree)->right != NULL)
            deleteTree(&(*givenTree)->right);
        free(*givenTree);
    }

#endif

// Źródła:
// http://en.wikibooks.org/wiki/Algorithm_Implementation/Sorting
// http://en.wikibooks.org/wiki/Algorithm_Implementation/Search
// http://algorytmy.blox.pl/2008/03/Wyszukiwanie-interpolacyjne.html
// http://www.geeksforgeeks.org/searching-for-patterns-set-3-rabin-karp-algorithm/
// http://kolos.math.uni.lodz.pl/~archive/Algorytmy%20i%20struktury%20danych%202%20%28Zaawansowane%20algorytmy%29/Projekt%204%20-%20Wzorzec/wyk12.pdf
// http://rosettacode.org/wiki/
// http://stackoverflow.com/questions/6579456/implementation-of-queue-using-pointers-segmentation-error
// http://en.wikipedia.org/wiki/Abstract_data_structure
// https://en.wikipedia.org/wiki/List_%28abstract_data_type%29
// http://en.wikipedia.org/wiki/List_of_data_structures#Binary_trees
// http://www.thegeekstuff.com/2013/02/c-binary-tree/
